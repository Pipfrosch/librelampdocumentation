EMOJI SOURCE
============

These Emoji are from Google's Noto Emoji font at Github -

https://github.com/googlefonts/noto-emoji

Apache License 2.0

See the `LICENSE` file in this directory, which is from that project.

The script `convert-emoji-to-png.sh` is mine - too trivial for license to matter,
just noting it is not from noto-emoji github.
