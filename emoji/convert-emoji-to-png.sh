#!/bin/bash

EMOJI="$1"
COLOR="`echo "${EMOJI}" |sed -e s?"\.svg$"?"-color.png"?`"
GRAY="`echo "${EMOJI}" |sed -e s?"\.svg$"?"-grayscale.png"?`"

inkscape "${EMOJI}" --export-width=256 --export-height=256 --export-png="${COLOR}"

convert -brightness-contrast 7x0 "${COLOR}" -colorspace Gray "${GRAY}"
