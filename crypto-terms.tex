% cryptography terms - no namespace used unless there is a conflict.
%  try to keep in alphabetical order by the name field.

% Numeric

% A
\longnewglossaryentry{argon2}
{
  name=Argon2,
  type=crypto
}
  {A family of \gls{passhash} designed by
  \myhref{https://en.wikipedia.org/wiki/Alex\_Biryukov}{Alex Biryukov},
  \myhref{https://www.linkedin.com/in/daniel-dinu-91338b17b}{Daniel Dinu}, and
  \myhref{https://en.wikipedia.org/wiki/Dmitry\_Khovratovich}{Dmitry Khovratovich}
  at the University of Luxembourg.

  The algorithm was designed specifically to be
  resistant to \gls{acr:gpu} cracking attacks. There are three variants in the
  family:

  \begin{description}
  \item[Argon2d]
  Maximizes resistance to \gls{acr:gpu} attacks by accessing the memory array in a
  password dependent order. This makes the algorithm more resistant to \gls{acr:tmto}
  attacks but at the cost of increased potential vulnerability to \glspl{sidechannel}.

  Generally this variant should only be used when you have physical security preventing
  unauthorized access to the room where your server resides.

  \item[Argon2i]
  Resists \glspl{sidechannel} by accessing the memory array in a password independent
  order but is not as resistant to \gls{acr:tmto}.

  \item[Argon2id]
  A hybrid of Argon2i and Argon2d. While it is not as resistant to \gls{acr:tmto} as
  Argon2d nor as resistant to \glspl{sidechannel} as Argon2i, it also is not as
  vulnerable to \glspl{sidechannel} as Argon2d or as vulnerable to \gls{acr:tmto} as
  Argon2i.

  Argon2id is my (Alice Wonder) personal choice for password hashing functions.
  \end{description}

  In 2015, Argon2 won the \myhref{https://password-hashing.net/}{Password Hashing Competition},
  an open competition announced in 2013 designed to find a new highly recommended
  \gls{passhash} that adequately defends against modern attack vectors.

  When \gls{acr:php} 7.2 and newer is compiled using the \texttt{-{-}with-password-argon2}
  configure flag it uses Argon2i as the default for the built-in PHP
  \myhref{https://www.php.net/manual/en/function.password-hash.php}{\texttt{password\_hash}} function.

  The \gls{acr:php} extension \myhref{https://paragonie.com/book/pecl-libsodium}{libsodium}
  provides access to all three variants and better defaults than the
  PHP built-in \texttt{password\_hash} function uses.
  }

\longnewglossaryentry{authcred}
{
  name=authentication credentials,
  type=crypto
}
  {An identity combined with a single-use token, long-term password or passphrase, or other mechanism by which a user or
  program authenticates itself to access a protected resource.}

% B
\longnewglossaryentry{bd}
{
  name=backdoor,
  type=crypto
}
  {An intentional method, usually code inserted into a program, that allows bypassing
  authentication or encyrption.

  Some hardware manufacturers will create backdoors in their hardware to make it easier
  for technicians to remotely access the device without needing authentication.

  Some government agencies will influence software developers to insert backdoors into
  software they write so that the government can access systems that use the software
  or messages that are encrypted
  using the software.

  Some \glspl{blackhat} will install backoors on systems they have compromised so that
  they can access the system again if they are discovered and the method they initially
  used to gain access is fixed.}

\longnewglossaryentry{bcrypt}
{
  name=bcrypt,
  type=crypto
}
  {A \gls{passhash} designed in 1999 that is based on the Blowfish cipher.

  bcrypt is an adaptive hash function, meaning you can increase the number of rounds
  used for the hash as hardware becomes faster, giving it resistance to brute force
  attacks attacks as long as the number of rounds used is
  increased to match increasing computing efficiency.

  bcrypt also uses a unique salt for each password making it very resistant to
  \gls{rainbowtable} attacks.}

\longnewglossaryentry{blackhat}
{
  name=black hat,
  type=crypto
}
  {A malicious system cracker (often referred to as a \emph{hacker} though not all
  hacking is related to system cracking) that attacks systems for nefarious purposes.

  Contrast with \gls{grayhat} and \gls{whitehat}.}

% C
\longnewglossaryentry{realcacert}
{
  name=CA Certificate,
  type=crypto
}
  {This can be confusing. This term is frequently used by the general public to indicate an \gls{x509} certificate that has been
  signed by a Certificate Authority (CA) but that is not how it is used in actual documentation.

  In actual documentation it refers to a certificate that is allowed to sign other certificates, a
  certificate that \emph{belongs} to a Certificate Authority. What we often call a root certificate or an
  intermediate certificate.}

\longnewglossaryentry{certauth}
{
  name=Certificate Authority,
  type=crypto
}
  {An entity (often a business) that vouches for the public key and associated
  metadata in an \gls{x509} certificate by signing a \gls{acr:csr} with their
  own private key to create a signed \gls{x509} certificate indicating they
  trust the public key is associated with the entity named in the \gls{acr:csr}
  (often a domain name but not always).

  When a message is signed using the private key associated with the public key
  in the \gls{acr:csr} that the Certificate Authority signed, other software and
  users that trust the Certificate Authority can then trust the authenticity of
  what was signed.}

\longnewglossaryentry{cert-trans}
{
  name=Certificate Transparency,
  type=crypto
}
  {Originally defined in \myrfc{6962}, Certificate Transparency is the practice of a \gls{acr:ca} making
  public records of every single certificate they issue.

  These logs can be scanned by automated tools to detect certificates that were issued but should not have
  been issued and also can be scanned for certificates issued to domain names that are very similar to
  domain names likely to be targets of a \gls{phishing} attack.

  Certificate Transparency has improved the security of the World Wide Web.}

\longnewglossaryentry{cryptohash}
{
  name=cryptographic hash function,
  type=crypto
}
  {A hash function, or algorithm, that has properties making it suitable for use
  in cryptography.

  They are one-way functions meaning you can not determine the data being hashed
  from the hash itself.

  They have the so-called `butterfly effect' (sometimes called `avalance effect')
  where the smallest change in the input being hashed results in an unpredictably
  different hash value. Given two hash values, it is not possible to know if the
  source input differed by a single bit or were completely different in every aspect.

  While mathematically \glspl{hashcollision} must exist, it is not possible to find them.
  If a hash collision is ever found, it indicates a serious flaw in the algorithm
  and it can no longer be used for cryptography purposes.}

% G
\longnewglossaryentry{grayhat}
{
  name=gray hat,
  type=crypto
}
  {A system cracker who often breaks laws but does not have a nefarious purpose to
  their system cracking. They do it to see if they can, for the challenge.

  Contrast with \gls{blackhat} and \gls{whitehat}.}

% H
\longnewglossaryentry{hashcollision}
{
  name=hash collision,
  type=crypto
}
  {A hash collision is where two different sets of input data result in the same
  generated hash value.}

% M
\longnewglossaryentry{md5}
{
  name=MD5,
  type=crypto
}
  {A 128-bit hash algorithm formerly used as a \gls{cryptohash}.

  It is seriously broken for cryptographic purposes with collisions now being cake to
  create.

  It still has valid uses, but not in cryptography.}

% N
\longnewglossaryentry{nonce}
{
  name=nonce,
  type=crypto
}
  {An expression used only once. When used in the context of \gls{acr:IT} security it is typically
  generated with the use of a secure \gls{pRNG} so it can not be predicted.

  Many are under the impression that nonce stands for `Number used Once' and while that is a good way
  to think of it, that is not actually the historical etymology, but rather mere coincidence.}

% O
\longnewglossaryentry{pgp}
{
  name=OpenPGP,
  type=crypto
}
  {Defined in \myrfc{4880}, OpenPGP is a social-based mechanism for managing trust in public keys
  associated with an identity.

  Unlike \gls{x509}, trust is not based upon trust in a \gls{acr:ca}. The user manages a key ring of
  keys they trust and decides whether or not to trust a key based upon the reputation of the
  key, how many keys with a good reputation vouch for the identity associated with the key. This
  is often referred to as a `Web of Trust'.

  I do not like the system because it has inherent bias in trust level based upon social circles
  and social skills. This makes it difficult for the socially challenged to obtain a good
  reputation for their keys but con artists often are very socially adept.

  There also is a tendency for users to trust an OpenPGP key simply becauase they want the resource
  that requires their trust. An example, if you want to use LibreLAMP you have to instruct
  \gls{acr:rpm} to import my RPM signing key into its keyring of trusted keys. Why do you trust
  it other than that it is there?

  \gls{acr:rpm} would be more secure if it required \gls{x509} validation before allowing a key
  to be imported in my opinion.}

% P
\longnewglossaryentry{passhash}
{
  name=password hash function,
  type=crypto
}
  {A type of \gls{cryptohash} that is well-suited for the purpose of password security. The hash must be
  resistant to the discovery of the password in the event the password database is compromised, a fairly
  common occurrence. In a perfect world the hash database would never be compromised but password hash
  functions must be designed for an imperfect world where that does indeed happen.

  In addition to being \gls{preimage} resistant, a password hashing function will be computationally
  expensive to reduce the effectiveness of brute force dictionary attacks. This means that \gls{cryptohash}
  functions that are suitable for message validation are generally not well suited for password hashes.

  A good password hash function will also generate a unique salt (stored with the hash) with each password
  hash generated as a very effective defense against \glspl{rainbowtable}.}

\longnewglossaryentry{passmanager}
{
  name=password manager,
  type=crypto
}
  {Software that helps users generate and store unique cryptographically strong authentication passwords.}

\longnewglossaryentry{phishing}
{
  name=phishing,
  type=crypto
}
  {A social engineering attack where the attacker tricks the victim into believing they are interacting
  with a legitimate web site when in fact they are interacting with a web site controlled by the attacker.

  Phishing attacks generally involve a domain name that is very similar but slightly different from the
  domain name used by the legitimate web site the attacker is pretending to be.}

\longnewglossaryentry{plaintext}
{
  name=plaintext,
  type=crypto
}
  {A message (whether actual text or binary content) that has not been obfuscated by encipherment, encoding, or hashing.}

\longnewglossaryentry{preimage}
{
  name=preimage attack,
  type=crypto
}
  {An attack that produces a message with a specific hash value, producing a crafted \gls{hashcollision}.}

\longnewglossaryentry{pRNG}
{
  name=pseudo-Random Number Generator,
  type=crypto
}
  {A \gls{acr:pRNG} is an algorithm that generates new values from a seed value that have the
  characteristics of a random sequence.

  When the same seed is used, the same sequence is produced, so technically speaking it is an
  algorithm produced sequence rather than a random sequence.

  With a cryptographically secure \gls{acr:pRNG} it is not possible to increase the odds of
  correctly guessing a number in the sequence based on any numbers that came before it or after
  it.

  A \gls{acr:pRNG} is usually seeded from the system entropy pool which uses a number of different
  methods to generate random data that is not from a seeded generator.}

% R
\longnewglossaryentry{rainbowtable}
{
  name=rainbow table,
  type=crypto
}
  {A rainbow table is a reverse lookup table where an attacker stores precomputed
  password hashes with the passwords they correspond with.

  Rainbow tables usually include every commonly used password hashed using a wide
  variety of algorithms, as well as any uncommon passwords the attacker has
  discovered.

  When an attacker manages to obtain a dump of password hashes associated with
  user accounts, the attacker will run the list through their rainbow table
  looking for matches.

  Users can defend against rainbow table attacks by always using unique strong
  passwords for each site, and system administrators can defend against rainbow
  table attacks by using unique \glspl{salt} for each password hash.}

\longnewglossaryentry{ripemd160}
{
  name=RIPEMD160,
  type=crypto
}
  {160-bit variant of \gls{acr:ripe} Message Digest family of \glspl{cryptohash}.
  The family was developed in 1992 as a project of the European Union. A collision
  has been found in the 128-bit variant of the family, but that collision does
  not effect the 160-bit variant, it is still considered secure.

  RIPEMD160 is not used very often, but it is used in the Bitcoin project during
  the process of generating an address from a private key.}

% S
\longnewglossaryentry{salt}
{
  name=salt,
  type=crypto
}
  {A salt is a usually gibberish string either prepended or appended to a meaningful
  string before a \gls{cryptohash} function is applied to it.

  The most common use case is with password hashes as a defense against \glspl{rainbowtable}
  but there are many other use cases for a salt when producing a data hash.

  In a nutshell, a salt changes the string to be hashed \emph{before} the hashing
  takes place.

  Thus to be effective, \glspl{rainbowtable} must be generated for each salt, radically
  reducing their efficiency.}

\longnewglossaryentry{skitty}
{
  name=script kitty,
  type=crypto
}
  {A version of Donald Trump Jr.\ who has had some limited exposure to Visual Basic, Python, PHP,
  or possibly Ruby.}

\longnewglossaryentry{sha1}
{
  name=SHA-1,
  type=crypto
}
  {A (formerly) \gls{cryptohash} designed by the \gls{acr:nsa} and released in 1995. This hash function
  produces a 160-bit hash typically displayed for humans in a 40 character length hexadecimal encoding.

  In 2005 it was suspected to have algorithm flaws. In 2017 a \gls{hashcollision} was
  \myhref{https://security.googleblog.com/2017/02/announcing-first-sha1-collision.html}{demonstrated}.

  This algorithm should not longer be used for cryptography or message authentication.}

\longnewglossaryentry{sha-2}
{
  name=SHA-2,
  type=crypto
}
  {A family of \glspl{cryptohash} designed by the \gls{acr:nsa} and released in 2001. The family
  contains six different variants:

  \begin{description}
  \item[SHA-224] A truncated version of the SHA-256 variant but using different initial values.
  As the name suggest, it produces a 224-bit hash.

  I have only seen this variant used once or twice on the open Internet and it seems to only be
  referenced in informational \myrfc{3874} and informational \myrfc{4634}. It is not used in
  any \gls{acr:ietf} standard that I am aware of.

  SHA-224 was proposed as an acceptable \gls{acr:mac}
  for \gls{acr:3des} cipher suites but I do not believe that ever took place and 3DES is no
  longer considered secure.

  \item[SHA-256] A 256-bit hash function that was rather quickly adopted in the \Unix{} world
  but did not have native support in Windows until Windows 7.

  Now that both \gls{md5} and \gls{sha1} are no longer considered secure, SHA-256 is the dominant
  algorithm used to validate message integrity including downloads and \gls{x509} certificates.
  It is also frequently used as a \gls{acr:mac} in \gls{acr:tls} cipher suites.

  When displayed for humans, a 64 character length hexadecimal encoding is usually used, though sometimes
  a 44 character length base64 encoding is used.

  \item[SHA-384] A truncated version of the SHA-512 variant but using different initial values.

  This variant is frequently used as a \gls{acr:mac} in \gls{acr:tls} cipher suites and for
  \gls{acr:sri} verification of resources fetched during the rendering of a web page.

  When displayed for humans, a 64 character length base64 encoding is usually used.

  \item[SHA-512] Very similar internally to SHA-256 but it uses 64-bit words instead of 32-bit
  words, different constants, and a few other minor internal differences.

  This variant sees signifcantly less use that SHA-256 or SHA-384.

  \item[SHA-512/224] A truncated version of SHA-512 but using initial values generated using
  a method described in \gls{acr:fips} PUB 180-4.

  This variant is not used by any \gls{acr:ietf} standard I am personally aware of. I have
  \emph{never} seen it used in the context of the Internet.

  \item[SHA-512/256] A truncated version of SHA-512 but using initial values generated using
  a method described in \gls{acr:fips} PUB 180-4.

  This variant is not used by any \gls{acr:ietf} standard I am personally aware of. I have
  \emph{never} seen it used in the context of the Internet.
  \end{description}}

\longnewglossaryentry{sidechannel}
{
  name=side channel attack,
  type=crypto
}
  {An attack on information security that targets the implementation of computer systems themselves
  rather than a weakness in software algorithms.}

% T
\longnewglossaryentry{sessionresumption}
{
  name=TLS session resumption,
  type=crypto
}
  {First defined in \myrfc{4507}, \gls{acr:tls}, session resumption allows a
  client that has recently connected to a TLS server to reconnect to the
  server by resuming the previous session thus avoiding the costly handshake
  negotiation required for a new session.

  This brings real performance benefits but it should be used with caution,
  TLS session resumption can result in privacy issues such as third party
  tracking even when a \gls{acr:vpn} is used and all browser cookies are
  cleared.

  With uses cases such as a mail server connecting to another mail server,
  these privacy concerns are not an issue. They are however an issue for
  web browsers where they can be abused to track users as a form of
  \gls{supercookie}.

  See {\scriptsize\url{https://www.privateinternetaccess.com/blog/2018/11/supercookey-a-supercookie-built-into-tls-1-2-and-1-3/}}}

\longnewglossaryentry{2fa}
{
  name=Two Factor Authentication,
  type=crypto
}
  {A security mechanism to reduce the impact of stolen \gls{authcred}. In addition to the primary
  mechanism of authentication, a second mechanism is used to reduce the possibility that a nefarious
  entity with stolen \gls{authcred} can access a protected resource.

  The second form of authentication usually but not always involves a \gls{nonce} that is generated
  after the first authentication mechanism is successful. The nonce is sent via a mechanism (such as text message on a phone)
  that only the legitimate user is likely to have access to.}

% W
\longnewglossaryentry{whitehat}
{
  name=white hat,
  type=crypto
}
  {A security researcher / penetration tester who does not (intentionally) violate
  any laws while looking for security vulnerabilities and how to prevent them.

  Contrast with \gls{grayhat} and \gls{blackhat}.}

% X
\longnewglossaryentry{x509}
{
   name=X.509,
   type=crypto
}
  {A cryptography standard for the management of public keys, validation of public keys, and limitations
  of what those public keys can be used for.

  X.509 is different than \gls{pgp} which uses social reputation to manage trust in public keys and does
  not facilitate limitations of what contexts a key should be limited for. Rather, X.509 uses a hierarchal
  `Chain of Trust' often referred to as \gls{acr:pkix}.

  Each X.509 certificate (often incorrectly referred to as a \gls{acr:tls} or \gls{acr:ssl} certificate)
  contains a public key from a cryptography key pair along with metadata parameters that specify who
  the associated private key belongs to,
  what contexts it is to be considered valid for, when the key expires, and usually a link to
  a \gls{acr:crl} or \gls{acr:ocsp} resource that can be used to verify that trust has not been revoked.

  The public key and associated metadata is then signed by the private key of an entity that vouches for
  the information in the X.509 certificate.

  When the \gls{acr:pkix} system trusts the signer of X.509 certificate (referred to as a \gls{acr:ca})
  and verifies the signature has not been revoked by that \gls{acr:ca},
  the X.509 certificate can be trusted and the public key can be used to validate data
  signed by the associated private key.}
