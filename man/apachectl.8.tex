%.TH "APACHECTL" 8 "2018-07-06" "Apache HTTP Server" "apachectl"

\chapter{apachectl -- Apache HTTP Server Control Interface}
\label{man:apachectl}

\section{SYNOPSIS}
\label{man:apachectl:synopsis}

\begin{hangparas}{3mm}{1} 
When acting in pass-through mode, \texttt{\textbf{apachectl}} can take all the arguments available for the httpd binary.

\bigskip 
\texttt{\textbf{apachectl}} \mbox{[~\underline{httpd-argument}~]}

\bigskip 
When acting in SysV init mode, \texttt{\textbf{apachectl}} takes simple, one-word commands, defined below.

\bigskip 
\texttt{\textbf{apachectl}} \texttt{\underline{command}}
\end{hangparas}

\bigskip
 
\section{SUMMARY}
\label{man:apachectl:summary}
 
\texttt{\textbf{apachectl}} is a front end to the Apache HyperText Transfer Protocol (HTTP) server.
It is designed to help the administrator control the functioning of the Apache httpd daemon.
 
The \texttt{\textbf{apachectl}} script can operate in two modes.
First, it can act as a simple front-end to the httpd command that simply sets any necessary environment variables and then invokes httpd,
passing through any command line arguments.
Second, \texttt{\textbf{apachectl}} can act as a SysV init script, taking simple one-word arguments like
\texttt{\textbf{start}},
\texttt{\textbf{restart}}, and
\texttt{\textbf{stop}},
and translating them into appropriate signals to httpd.
 
If your Apache installation uses non-standard paths, you will need to edit the \texttt{\textbf{apachectl}} script to set the appropriate paths to the httpd binary.
You can also specify any necessary httpd command line arguments.
See the comments in the script for details.
 
The \texttt{\textbf{apachectl}} script returns a \texttt{0} exit value on success,
and \texttt{>0} if an error occurs.
For more details, view the comments in the script.

\newpage
 
\section{OPTIONS}
 
Only the SysV init-style options are defined here. Other arguments are defined on the \myhref{\apachemanual{}programs/httpd.html}{httpd manual} page.
 
\begin{description}

\item[\texttt{start}]
Start the Apache httpd daemon.
Gives an error if it is already running.
This is equivalent to \mbox{\texttt{\textbf{apachectl~-k~start}}}.  

\item[\texttt{stop}]
Stops the Apache httpd daemon.
This is equivalent to \mbox{\texttt{\textbf{apachectl~-k~stop}}}.  

\item[\texttt{restart}]
Restarts the Apache httpd daemon.
If the daemon is not running, it is started.
This command automatically checks the configuration files as in \texttt{\textbf{configtest}} before initiating the restart to make sure the daemon doesn't die.
This is equivalent to \mbox{\texttt{\textbf{apachectl~-k~restart}}}.  

\item[\texttt{fullstatus}]
Displays a full status report from \texttt{mod\_status}.
For this to work, you need to have \texttt{mod\_status} enabled on your server and a text-based browser such as
\texttt{\textbf{lynx}} available on your system.
The \gls{acr:url} used to access the status report can be set by editing the \texttt{\textbf{STATUSURL}} variable in the script.  

\item[\texttt{status}]
Displays a brief status report.
Similar to the \texttt{\textbf{fullstatus}} option, except that the list of requests currently being served is omitted.  

\item[\texttt{graceful}]
Gracefully restarts the Apache httpd daemon.
If the daemon is not running, it is started.
This differs from a normal restart in that currently open connections are not aborted.
A side effect is that old log files will not be closed immediately.
This means that if used in a log rotation script, a substantial delay may be necessary to ensure that the old log files are closed before processing them.
This command automatically checks the configuration files as in \texttt{\textbf{configtest}} before initiating the restart to make sure Apache doesn't die.
This is equivalent to \mbox{\texttt{\textbf{apachectl~-k~graceful}}}.  

\item[\texttt{graceful-stop}]
Gracefully stops the Apache httpd daemon.
This differs from a normal stop in that currently open connections are not aborted.
A side effect is that old log files will not be closed immediately.
This is equivalent to \mbox{\texttt{\textbf{apachectl~-k~graceful-stop}}}.  

\item[\texttt{configtest}]
Run a configuration file syntax test.
It parses the configuration files and either reports \texttt{\textbf{Syntax Ok}} or detailed information about the particular syntax error.
This is equivalent to \mbox{\texttt{\textbf{apachectl~-t}}}.
\end{description} 
 
\bigskip
The following option was available in earlier versions but has been removed.
 
\begin{description}
\item[\texttt{startssl}]
To start httpd with SSL support, you should edit your configuration file to include the relevant directives and then use the normal
\mbox{\texttt{\textbf{apachectl~start}}}.
\end{description}

\section{Man Page Notes and Legal}
\label{man:apachectl:legal}

The \LaTeX{} source for this man page started as the man page that shipped with Apache
httpd 2.4.39 and can be revision identified from the following information:

\begin{ManVerbatim}
.TH "APACHECTL" 8 "2018-07-06" "Apache HTTP Server" "apachectl"
\end{ManVerbatim}

The original man page source file did not contain author or license information, so the
Apache 2.0 license for the Apache httpd project is applicable.
See page~\pageref{license:apache}. 
 
