% work in progress

\chapter{X.509 V3 certificate extension configuration format}
\label{man:x509v3.cnf}

\section{DESCRIPTION}
\label{man:x509v3.cnf:description}

Several of the OpenSSL utilities can add extensions to a certificate or
certificate request based on the contents of a configuration file.
\newline
The file format is based on the
\texttt{libressl.cnf(5)} (see appendix~\ref{man:libressl.cnf} on page~\pageref{man:libressl.cnf})
format.

Typically the application will contain an option to point to an
extension section.

Each line of the extension section takes the form:

\begin{ManVerbatim}
extension_name=[critical,] extension_options
\end{ManVerbatim}

If
\textbf{\texttt{critical}}
is present, then the extension will be critical.

The format of
\underline{\texttt{extension\_options}}
depends on the value of
\underline{\texttt{extension\_name}}.

There are four main types of extension: string extensions, multi-valued
extensions, raw extensions, and arbitrary extensions.

String extensions simply have a string which contains either the value
itself or how it is obtained.
For example:

\begin{ManVerbatim}
nsComment="This is a Comment"
\end{ManVerbatim}

Multi-valued extensions have a short form and a long form.
The short form is a list of names and values:

\begin{ManVerbatim}
basicConstraints=critical,CA:true,pathlen:1
\end{ManVerbatim}

The long form allows the values to be placed in a separate section:

\begin{ManVerbatim}
basicConstraints=critical,@bs_section

[bs_section]
CA=true
pathlen=1
\end{ManVerbatim}

Both forms are equivalent.

The syntax of raw extensions is governed by the extension code:
it can for example contain data in multiple sections.
The correct syntax to use is defined by the extension code itself:
check out the certificate policies extension for an example.

If an extension type is unsupported, then the arbitrary extension
syntax must be used; see the
ARBITRARY EXTENSIONS
section (section \ref{man:x509v3.cnf:arbitrary-extensions} on page~\pageref{man:x509v3.cnf:arbitrary-extensions})
for more details.

\section{STANDARD EXTENSIONS}
\label{man:x509v3.cnf:standard-extensions}

The following sections describe each supported extension in detail.

\subsection{Basic constraints}
This is a multi-valued extension which indicates whether a certificate
is a \gls{realcacert}.
The first (mandatory) name is
\textbf{\texttt{CA}}
followed by
\textbf{\texttt{TRUE}}
or
\textbf{\texttt{FALSE}}.
If
\textbf{\texttt{CA}}
is
\textbf{\texttt{TRUE}},
then an optional
\textbf{\texttt{pathlen}}
name followed by a non-negative value can be included.
For example:

\begin{ManVerbatim}
basicConstraints=CA:TRUE
basicConstraints=CA:FALSE
basicConstraints=critical,CA:TRUE, pathlen:0
\end{ManVerbatim}

A \gls{realcacert} must include the
\textbf{\texttt{basicConstraints}}
value with the
\textbf{\texttt{CA}}
field set to
\textbf{\texttt{TRUE}}.
An end user certificate must either set
\textbf{\texttt{CA}}
to
\textbf{\texttt{FALSE}}
or exclude the extension entirely.
Some software may require the inclusion of
\textbf{\texttt{basicConstraints}}
with
\textbf{\texttt{CA}}
set to
\textbf{\texttt{FALSE}}
for end entity certificates.

The
\textbf{\texttt{pathlen}}
parameter indicates the maximum number of \gls{acr:ca}s that can appear below
this one in a chain.
So if you have a \gls{acr:ca} with a
\textbf{\texttt{pathlen}}
of zero it can only be used to sign end user certificates and not
further \glspl{realcacert}.

\subsection{Key usage}

Key usage is a multi-valued extension consisting of a list of names of
the permitted key usages.

The supported names are:
\textbf{\texttt{digitalSignature}},
\textbf{\texttt{nonRepudiation}},
\textbf{\texttt{keyEncipherment}},
\textbf{\texttt{dataEncipherment}},
\textbf{\texttt{keyAgreement}},
\textbf{\texttt{keyCertSign}},
\textbf{\texttt{cRLSign}},
\textbf{\texttt{encipherOnly}},
and
\textbf{\texttt{decipherOnly}}.
Examples:

\begin{ManVerbatim}
keyUsage=digitalSignature, nonRepudiation
keyUsage=critical, keyCertSign
\end{ManVerbatim}

\subsection{Extended key usage}

This extensions consists of a list of usages indicating purposes for
which the certificate public key can be used for.

These can either be object short names or the dotted numerical form of \gls{acr:oid}s.
While any \gls{acr:oid} can be used, only certain values make sense.
In particular the following \gls{acr:pkix}, NS and MS values are meaningful:

\vspace{2mm}

\begin{tabular}{l l}
\vspace{1mm}
\underline{value}                 & \underline{meaning} \\
\textbf{\texttt{serverAuth}}      & SSL/TLS web server authentication \\
\textbf{\texttt{clientAuth}}      & SSL/TLS web client authentication \\
\textbf{\texttt{codeSigning}}     & code signing \\
\textbf{\texttt{emailProtection}} & E-mail protection (S/MIME) \\
\textbf{\texttt{timeStamping}}    & trusted timestamping \\
\textbf{\texttt{OCSPSigning}}     & \gls{acr:ocsp} signing \\
\textbf{\texttt{ipsecIKE}}        & IPsec internet key exchange \\
\textbf{\texttt{msCodeInd}}       & Microsoft individual code signing (authenticode) \\
\textbf{\texttt{msCodeCom}}       & Microsoft commercial code signing (authenticode) \\
\textbf{\texttt{msCTLSign}}       & Microsoft trust list signing \\
\textbf{\texttt{msEFS}}           & Microsoft encrypted file system \\
\end{tabular}

\vspace{2mm}

Examples:

\begin{ManVerbatim}
extendedKeyUsage=critical,codeSigning,1.2.3.4
extendedKeyUsage=serverAuth,clientAuth
\end{ManVerbatim}

\subsection{Subject key identifier}

This is really a string extension and can take two possible values.
Either the word
\textbf{\texttt{hash}}
which will automatically follow the guidelines in \myrfc{3280}
or a hex string giving the extension value to include.
The use of the hex string is strongly discouraged.
Example:

\begin{ManVerbatim}
subjectKeyIdentifier=hash
\end{ManVerbatim}

\subsection{Authority key identifier}

The authority key identifier extension permits two options,
\textbf{\texttt{keyid}}
and
\textbf{\texttt{issuer}}:
both can take the optional value
\textbf{\texttt{always}}.

If the
\textbf{\texttt{keyid}}
option is present, an attempt is made to copy the subject
key identifier from the parent certificate.
If the value
\textbf{\texttt{always}}
is present, then an error is returned if the option fails.

The
\textbf{\texttt{issuer}}
option copies the issuer and serial number from the issuer certificate.
This will only be done if the
\textbf{\texttt{keyid}}
option fails or is not included unless the
\textbf{\texttt{always}}
flag will always include the value.
Example:

\begin{ManVerbatim}
authorityKeyIdentifier=keyid,issuer
\end{ManVerbatim}

\subsection{Subject alternative name}
\label{man:x509v3.cnf:description:san}

The subject alternative name extension allows various literal values to
be included in the configuration file.
These include
\textbf{\texttt{email}}
(an email address),
\textbf{\texttt{URI}}
(a uniform resource indicator),
\textbf{\texttt{DNS}}
(a DNS domain name),
\textbf{\texttt{RID}}
(a registered ID: OBJECT IDENTIFIER),
\textbf{\texttt{IP}}
(an IP address),
\textbf{\texttt{dirName}}
(a distinguished name), and
\textbf{\texttt{otherName}}.

The
\textbf{\texttt{email}}
option can include a special
\textbf{\texttt{copy}}
value.
This will automatically include any email addresses contained in the
certificate subject name in the extension.

The IP address used in the
\textbf{\texttt{IP}}
options can be in either IPv4 or IPv6 format.

The value of
\textbf{\texttt{dirName}}
should point to a section containing the distinguished name to use as a
set of name value pairs.
Multi values AVAs can be formed by prefacing the name with a
`\texttt{+}'
character.

\textbf{\texttt{otherName}}
can include arbitrary data associated with an \gls{acr:oid}: the value should
be the \gls{acr:oid} followed by a semicolon and the content in standard
\texttt{ASN1\_generate\_nconf(3)}
format.
Examples:

\begin{ManVerbatim}
subjectAltName=email:copy,email:my@other.address,URI:http://my.url.here/
subjectAltName=IP:192.168.7.1
subjectAltName=IP:13::17
subjectAltName=email:my@other.address,RID:1.2.3.4
subjectAltName=otherName:1.2.3.4;UTF8:some other identifier

subjectAltName=dirName:dir_sect

[dir_sect]
C=UK
O=My Organization
OU=My Unit
CN=My Name
\end{ManVerbatim}

\subsection{Issuer alternative name}

The issuer alternative name option supports all the literal options of
subject alternative name.
It does not support the
\textbf{\texttt{email:copy}}
option because that would not make sense.
It does support an additional
\textbf{\texttt{issuer:copy}}
option that will copy all the subject alternative name values from
the issuer certificate (if possible).
Example:

\begin{ManVerbatim}
issuerAltName = issuer:copy
\end{ManVerbatim}

\subsection{Authority info access}

The authority information access extension gives details about how to
access certain information relating to the \gls{acr:ca}.
Its syntax is
\underline{\texttt{accessOID}}; \underline{\texttt{location}}
where
\underline{\texttt{location}}
has the same syntax as subject alternative name (except that
\textbf{\texttt{email:copy}}
is not supported).
\underline{\texttt{accessOID}}
can be any valid \gls{acr:oid} but only certain values are meaningful,
for example
\textbf{\texttt{OCSP}}
and
\textbf{\texttt{caIssuers}}.
Example:

\begin{ManVerbatim}
authorityInfoAccess = OCSP;URI:http://ocsp.my.host/
authorityInfoAccess = caIssuers;URI:http://my.ca/ca.html
\end{ManVerbatim}

\subsection{CRL distribution points}

This is a multi-valued extension whose options can be either in
\texttt{\underline{name}:\underline{value}}
pair form using the same form as subject alternative name or a
single value representing a section name containing all the
distribution point fields.

For a
\texttt{\underline{name}:\underline{value}}
pair a new DistributionPoint with the fullName field set to the
given value, both the cRLissuer and reasons fields are omitted in
this case.

In the single option case, the section indicated contains values
for each field.
In this section:

If the name is
\textbf{\texttt{fullname}},
the value field should contain the full name of the distribution
point in the same format as subject alternative name.

If the name is
\textbf{\texttt{relativename}},
then the value field should contain a section name whose contents
represent a \texttt{DN} fragment to be placed in this field.

The name
\textbf{\texttt{CRLIssuer}},
if present, should contain a value for this field in subject
alternative name format.

If the name is
\textbf{\texttt{reasons}},
the value field should consist of a comma separated field containing
the reasons.
Valid reasons are:
\textbf{\texttt{keyCompromise}},
\textbf{\texttt{CACompromise}},
\textbf{\texttt{affiliationChanged}},
\textbf{\texttt{superseded}},
\textbf{\texttt{cessationOfOperation}},
\textbf{\texttt{certificateHold}},
\textbf{\texttt{privilegeWithdrawn}},
and
\textbf{\texttt{AACompromise}}.

Simple examples:

\begin{ManVerbatim}
crlDistributionPoints=URI:http://myhost.com/myca.crl
crlDistributionPoints=URI:http://my.com/my.crl,URI:http://oth.com/my.crl
\end{ManVerbatim}

Full distribution point example:

\begin{ManVerbatim}
crlDistributionPoints=crldp1_section

[crldp1_section]
fullname=URI:http://myhost.com/myca.crl
CRLissuer=dirName:issuer_sect
reasons=keyCompromise, CACompromise

[issuer_sect]
C=UK
O=Organisation
CN=Some Name
\end{ManVerbatim}

\subsection{Issuing distribution point}

This extension should only appear in CRLs.
It is a multi-valued extension whose syntax is similar to the "section"
pointed to by the \gls{acr:crl} distribution points extension with a few
differences.

The names
\textbf{\texttt{reasons}}
and
\textbf{\texttt{CRLissuer}}
are not recognized.

The name
\textbf{\texttt{onlysomereasons}}
is accepted, which sets this field.
The value is in the same format as the \gls{acr:crl} distribution point
\textbf{\texttt{reasons}}
field.

The names
\textbf{\texttt{onlyuser}},
\textbf{\texttt{onlyCA}},
\textbf{\texttt{onlyAA}},
and
\textbf{\texttt{indirectCRL}}
are also accepted.
The values should be a boolean values
(\textbf{\texttt{TRUE}}
or
\textbf{\texttt{FALSE}})
to indicate the value of the corresponding field.
Example:

\begin{ManVerbatim}
issuingDistributionPoint=critical, @idp_section

[idp_section]
fullname=URI:http://myhost.com/myca.crl
indirectCRL=TRUE
onlysomereasons=keyCompromise, CACompromise

[issuer_sect]
C=UK
O=Organisation
CN=Some Name
\end{ManVerbatim}

\subsection{Certificate policies}

This is a raw extension.
All the fields of this extension can be set by using the appropriate
syntax.

If you follow the \gls{acr:pkix} recommendations and just use one \gls{acr:oid}, then you
just include the value of that \gls{acr:oid}.
Multiple \gls{acr:oid}s can be set separated by commas, for example:

\begin{ManVerbatim}
certificatePolicies= 1.2.4.5, 1.1.3.4
\end{ManVerbatim}

If you wish to include qualifiers, then the policy \gls{acr:oid} and qualifiers
need to be specified in a separate section: this is done by using the
\texttt{@\underline{section}}
syntax instead of a literal \gls{acr:oid} value.

The section referred to must include the policy \gls{acr:oid} using the name
\textbf{\texttt{policyIdentifier}}.
\textbf{\texttt{CPSuri}}
qualifiers can be included using the syntax:

\begin{ManVerbatim}
CPS.nnn=value
\end{ManVerbatim}

\textbf{\texttt{userNotice}}
qualifiers can be set using the syntax:

\begin{ManVerbatim}
userNotice.nnn=@notice
\end{ManVerbatim}

The value of the
\textbf{\texttt{userNotice}}
qualifier is specified in the relevant section.
This section can include
\textbf{\texttt{explicitText}},
\textbf{\texttt{organization}},
and
\textbf{\texttt{noticeNumbers}}
options.
\textbf{\texttt{explicitText}}
and
\textbf{\texttt{organization}}
are text strings,
and
\textbf{\texttt{noticeNumbers}}
is a comma separated list of numbers.
The
\textbf{\texttt{organization}}
and
\textbf{\texttt{noticeNumbers}}
options (if included) must
\emph{both}
be present.
If you use the
\textbf{\texttt{userNotice}}
option with IE5 then you need the
\textbf{\texttt{ia5org}}
option at the top level to modify the encoding: otherwise it will
not be interpreted properly.
Example:

\begin{ManVerbatim}
certificatePolicies=ia5org,1.2.3.4,1.5.6.7.8,@polsect

[polsect]
policyIdentifier = 1.3.5.8
CPS.1="http://my.host.name/"
CPS.2="http://my.your.name/"
userNotice.1=@notice

[notice]
explicitText="Explicit Text Here"
organization="Organisation Name"
noticeNumbers=1,2,3,4
\end{ManVerbatim}

The
\textbf{\texttt{ia5org}}
option changes the type of the
\textbf{\texttt{organization}}
field.
In \myrfc{2459}, it can only be of type
\underline{\texttt{DisplayText}}.
In \myrfc{3280},
\underline{\texttt{IA5String}}
is also permissible.
Some software (for example some versions of MSIE) may require
\textbf{\texttt{ia5org}}.

\subsection{Policy constraints}

This is a multi-valued extension which consists of the names
\textbf{\texttt{requireExplicitPolicy}}
or
\textbf{\texttt{inhibitPolicyMapping}}
and a non-negative integer value.
At least one component must be present.
Example:

\begin{ManVerbatim}
policyConstraints = requireExplicitPolicy:3
\end{ManVerbatim}

\subsection{Inhibit any policy}

This is a string extension whose value must be a non-negative integer.
Example:

\begin{ManVerbatim}
inhibitAnyPolicy = 2
\end{ManVerbatim}

\subsection{Name constraints}

The name constraints extension is a multi-valued extension.
The name should begin with the word
\textbf{\texttt{permitted}}
or
\textbf{\texttt{excluded}},
followed by a semicolon.
The rest of the name and the value follows the syntax of \texttt{subjectAltName}
except
\textbf{\texttt{email:copy}}
is not supported and the
\textbf{\texttt{IP}}
form should consist of an IP addresses and subnet mask separated
by a slash.
Examples:

\begin{ManVerbatim}
nameConstraints=permitted;IP:192.168.0.0/255.255.0.0
nameConstraints=permitted;email:.somedomain.com
nameConstraints=excluded;email:.com
\end{ManVerbatim}

\subsection{OCSP no check}

The \gls{acr:ocsp} no check extension is a string extension,
but its value is ignored.
Example:

\begin{ManVerbatim}
noCheck = ignored
\end{ManVerbatim}

\subsection{TLS Feature (aka must staple)}
\label{man:x509v3.cnf:standard-extensions:tls-feature}

This is a multi-valued extension consisting of a list of TLS extension
identifiers.
Each identifier may be a number in the range from 0 to 65535 or a
supported name.
When a TLS client sends a listed extension, the TLS server is expected
to include that extension in its reply.

The supported names are:
\textbf{\texttt{status\_request}}
and
\textbf{\texttt{status\_request\_v2}}.
Example:

\begin{ManVerbatim}
tlsfeature = status_request
\end{ManVerbatim}

\section{DEPRECATED EXTENSIONS}
\label{man:x509v3.cnf:deprecated-extensions}

The following extensions are non-standard, Netscape specific and largely
obsolete.
Their use in new applications is discouraged.

\subsection{Netscape string extensions}

Netscape comment
(\textbf{\texttt{nsComment}})
is a string extension containing a comment which will be displayed when
the certificate is viewed in some browsers.
Example:

\begin{ManVerbatim}
nsComment = "Some Random Comment"
\end{ManVerbatim}

Other supported extensions in this category are:
\textbf{\texttt{nsBaseUrl}},
\textbf{\texttt{nsRevocationUrl}},
\textbf{\texttt{nsCaRevocationUrl}},
\textbf{\texttt{nsRenewalUrl}},
\textbf{\texttt{nsCaPolicyUrl}},
and
\textbf{\texttt{nsSslServerName}}.

\subsection{Netscape certificate type}

This is a multi-valued extensions which consists of a list of flags to
be included.
It was used to indicate the purposes for which a certificate could be
used.
The
\textbf{\texttt{basicConstraints}},
\textbf{\texttt{keyUsage}},
and extended key usage extensions are now used instead.

Acceptable values for
\textbf{\texttt{nsCertType}}
are:
\textbf{\texttt{client}},
\textbf{\texttt{server}},
\textbf{\texttt{email}},
\textbf{\texttt{objsign}},
\textbf{\texttt{reserved}},
\textbf{\texttt{sslCA}},
\textbf{\texttt{emailCA}},
\textbf{\texttt{objCA}}.

\section{ARBITRARY EXTENSIONS}
\label{man:x509v3.cnf:arbitrary-extensions}

If an extension is not supported by the OpenSSL code, then it must
be encoded using the arbitrary extension format.
It is also possible to use the arbitrary format for supported
extensions.
Extreme care should be taken to ensure that the data is formatted
correctly for the given extension type.

There are two ways to encode arbitrary extensions.

The first way is to use the word
\textbf{\texttt{ASN1}}
followed by the extension content using the same syntax as
\texttt{ASN1\_generate\_nconf(3)} .
For example:

\begin{ManVerbatim}
1.2.3.4=critical,ASN1:UTF8String:Some random data
1.2.3.4=ASN1:SEQUENCE:seq_sect

[seq_sect]
field1 = UTF8:field1
field2 = UTF8:field2
\end{ManVerbatim}

It is also possible to use the word
\textbf{\texttt{DER}}
to include the raw encoded data in any extension.

\begin{ManVerbatim}
1.2.3.4=critical,DER:01:02:03:04
1.2.3.4=DER:01020304
\end{ManVerbatim}

The value following
\textbf{\texttt{DER}}
is a hex dump of the \gls{acr:der} encoding of the extension.
Any extension can be placed in this form to override the default behaviour.
For example:

\begin{ManVerbatim}
basicConstraints=critical,DER:00:01:02:03
\end{ManVerbatim}

\section{FILES}
\label{man:x509v3.cnf:files}

\begin{description}
\item[\texttt{/etc/pki/tls/x509v3.cnf}]
standard configuration file
\end{description}

\section{SEE ALSO}
\label{man:x509v3.cnf:see-also}

\begin{itemize}
\item \texttt{libressl(1)}
\item \texttt{ASN1\_generate\_nconf(3)}
\item \texttt{libressl.cnf(5)} (appendix~\ref{man:libressl.cnf} on page~\pageref{man:libressl.cnf})
\end{itemize}

\section{HISTORY}
\label{man:x509v3.cnf:history}

X509v3 extension code was first added to OpenSSL 0.9.2.

\section{CAVEATS}
\label{man:x509v3.cnf:caveats}

There is no guarantee that a specific implementation will process a
given extension.
It may therefore sometimes be possible to use certificates for purposes
prohibited by their extensions because a specific application does not
recognize or honour the values of the relevant extensions.

The
\textbf{\texttt{DER}}
and
\textbf{\texttt{ASN1}}
options should be used with caution.
It is possible to create totally invalid extensions if they are not used
carefully.

If an extension is multi-value and a field value must contain a comma,
the long form must be used.
Otherwise the comma would be misinterpreted as a field separator.
For example,

\begin{ManVerbatim}
subjectAltName=URI:ldap://somehost.com/CN=foo,OU=bar
\end{ManVerbatim}

will produce an error, but the following form is valid:

\begin{ManVerbatim}
subjectAltName=@subject_alt_section

[subject_alt_section]
subjectAltName=URI:ldap://somehost.com/CN=foo,OU=bar
\end{ManVerbatim}

Due to the behaviour of the OpenSSL CONF library, the same field
name can only occur once in a section.
That means that

\begin{ManVerbatim}
subjectAltName=@alt_section

[alt_section]
email=steve@here
email=steve@there
\end{ManVerbatim}

will only use the last value.
This can be worked around by using the form:

\begin{ManVerbatim}
[alt_section]
email.1=steve@here
email.2=steve@there
\end{ManVerbatim}

\section{Man Page Notes and Legal}
\label{man:x509v3.cnf:legal}

The \LaTeX{} source for this man page started as the man page that shipped with
LibreSSL 2.9.2 and can be revision identified from the following information:

\begin{ManVerbatim}
.\" $OpenBSD: x509v3.cnf.5,v 1.5 2018/08/26 18:04:54 jmc Exp $
.\" full merge up to:
.\" OpenSSL man5/x509v3_config a41815f0 Mar 17 18:43:53 2017 -0700
.\" selective merge up to: OpenSSL 36cf10cf Oct 4 02:11:08 2017 -0400
\end{ManVerbatim}

This file was written by Dr. Stephen Henson [steve@openssl.org].
Copyright \copyright{} 2004, 2006, 2013, 2014, 2015, 2016 The OpenSSL Project.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

\begin{enumerate}
\item Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

\item Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

\item All advertising materials mentioning features or use of this
    software must display the following acknowledgment:
    ``This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit. (\url{https://www.openssl.org/})''

\item The names ``OpenSSL Toolkit'' and ``OpenSSL Project'' must not be used to
    endorse or promote products derived from this software without
    prior written permission. For written permission, please contact
    openssl-core@openssl.org.

\item Products derived from this software may not be called ``OpenSSL''
    nor may ``OpenSSL'' appear in their names without prior written
    permission of the OpenSSL Project.

\item Redistributions of any form whatsoever must retain the following
    acknowledgment:
    ``This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit (\url{https://www.openssl.org/})''
\end{enumerate}

THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
