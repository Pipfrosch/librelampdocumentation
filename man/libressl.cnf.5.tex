\chapter{LibreSSL configuration files}
\label{man:libressl.cnf}

\section{DESCRIPTION}
\label{man:libressl.cnf:description}

The OpenSSL \texttt{CONF} library can be used to read configuration files; see
\texttt{CONF\_modules\_load\_file(3)}.
It is used for the OpenSSL master configuration file
\underline{\texttt{/etc/pki/tls/libressl.cnf}}
and in a few other places like
\textbf{\texttt{SPKAC}}
files and certificate extension files for the
\texttt{openssl(1)}
\textbf{\texttt{x509}}
utility.
OpenSSL applications can also use the \texttt{CONF} library for their own
purposes.

A configuration file is divided into a number of sections.
Each section starts with a line
\texttt{[\underline{section\_name}]}
and ends when a new section is started or the end of the file is reached.
A section name can consist of alphanumeric characters and underscores.

The first section of a configuration file is special and is referred to
as the
``default section''.
It is usually unnamed and extends from the start of file to the
first named section.
When a name is being looked up, it is first looked up in a named
section (if any) and then in the default section.

The environment is mapped onto a section called
\textbf{\texttt{ENV}}.

Comments can be included by preceding them with the
`\texttt{\#}'
character.

Each section in a configuration file consists of a number of name and
value pairs of the form
\texttt{\underline{name}=\underline{value}}.

The
\texttt{\underline{name}}
string can contain any alphanumeric characters as well as a few
punctuation symbols such as
`\texttt{.}'
`\texttt{,}'
`\texttt{;}'
and
`\texttt{\_}'.

The
\texttt{\underline{value}}
string consists of the string following the
`\texttt{=}'
character until the end of the line with any leading and trailing
whitespace removed.

The value string undergoes variable expansion.
This can be done by including substrings of the form
\texttt{\$\underline{name}}
or
\texttt{\$\string{\underline{name}\string}}:
this will substitute the value of the named variable in the current
section.
It is also possible to substitute a value from another section using the
syntax
\texttt{\$\underline{section}::\underline{name}}
or
\texttt{\$\string{\underline{section}::\underline{name}\string}}.
By using the form
\texttt{\$\textbf{ENV}::\underline{name}},
environment variables can be substituted.
It is also possible to assign values to environment variables by using
the name
\texttt{\textbf{ENV}::\underline{name}}.
This will work if the program looks up environment variables using
the CONF library instead of calling
\texttt{getenv(3)}
directly.

It is possible to escape certain characters by using any kind of quote
or the
`\texttt{\textbackslash{}}'
character.
By making the last character of a line a
`\texttt{\textbackslash{}}',
a
\texttt{\underline{value}}
string can be spread across multiple lines.
In addition the sequences
`\texttt{\textbackslash{}n}',
`\texttt{\textbackslash{}r}',
`\texttt{\textbackslash{}b}',
and
`\texttt{\textbackslash{}t}',
are recognized.

\section{OPENSSL LIBRARY CONFIGURATION}
\label{man:libressl.cnf:openssl-library-configuration}

Applications can automatically configure certain aspects of OpenSSL
using the master OpenSSL configuration file, or optionally an
alternative configuration file.
The
\texttt{libressl}
utility includes this functionality: any sub command uses the master
OpenSSL configuration file unless an option is used in the sub command
to use an alternative configuration file.

To enable library configuration, the default section needs to contain
an appropriate line which points to the main configuration section.
The default name is
\textbf{\texttt{openssl\_conf}},
which is used by the
\texttt{libressl}
utility.
Other applications may use an alternative name such as
\textbf{\texttt{myapplication\_conf}}.
All library configuration lines appear in the default section
at the start of the configuration file.

The configuration section should consist of a set of name value pairs
which contain specific module configuration information.
The
\texttt{\underline{name}}
represents the name of the configuration module.
The meaning of the
\texttt{\underline{value}}
is module specific: it may, for example, represent a further
configuration section containing configuration module specific
information.
For example:

\begin{ManVerbatim}
# The following line must be in the default section.
openssl_conf = openssl_init

[openssl_init]
oid_section = new_oids
engines = engine_section

[new_oids]
... new oids here ...

[engine_section]
... engine stuff here ...
\end{ManVerbatim}

The features of each configuration module are described below.

\subsection{ASN1 Object Configuration Module}

This module has the name
\textbf{\texttt{oid\_section}}.
The value of this variable points to a section containing name value
pairs of OIDs: the name is the \gls{acr:oid} short and long name, and the value is the
numerical form of the \gls{acr:oid}.
Although some of the
\texttt{libressl}
utility subcommands already have their own \texttt{ASN1 OBJECT} section
functionality, not all do.
By using the \texttt{ASN1 OBJECT} configuration module, all the
\texttt{libressl}
utility subcommands can see the new objects as well as any compliant
applications.
For example:

\begin{ManVerbatim}
[new_oids]
some_new_oid = 1.2.3.4
some_other_oid = 1.2.3.5
\end{ManVerbatim}

It is also possible to set the value to the long name followed by a
comma and the numerical \gls{acr:oid} form.
For example:

\begin{ManVerbatim}
shortName = some object long name, 1.2.3.4
\end{ManVerbatim}

\subsection{Engine Configuration Module}

This \texttt{ENGINE} configuration module has the name
\textbf{\texttt{engines}}.
The value of this variable points to a section containing further \texttt{ENGINE}
configuration information.

The section pointed to by
\textbf{\texttt{engines}}
is a table of engine names (though see
\textbf{\texttt{engine\_id}}
below) and further sections containing configuration information
specific to each \texttt{ENGINE}.

Each \texttt{ENGINE} specific section is used to set default algorithms, load
dynamic \texttt{ENGINE}s, perform initialization and send ctrls.
The actual operation performed depends on the command
name which is the name of the name value pair.
The currently supported commands are listed below.

For example:

\begin{ManVerbatim}
[engine_section]
# Configure ENGINE named "foo"
foo = foo_section
# Configure ENGINE named "bar"
bar = bar_section

[foo_section]
... foo ENGINE specific commands ...

[bar_section]
... "bar" ENGINE specific commands ...
\end{ManVerbatim}

The command
\textbf{\texttt{engine\_id}}
is used to give the \texttt{ENGINE} name.
If used this command must be first.
For example:

\begin{ManVerbatim}
[engine_section]
# This would normally handle an ENGINE named "foo"
foo = foo_section

[foo_section]
# Override default name and use "myfoo" instead.
engine_id = myfoo
\end{ManVerbatim}

The command
\textbf{\texttt{dynamic\_path}}
loads and adds an \texttt{ENGINE} from the given path.
It is equivalent to sending the ctrls
\textbf{\texttt{SO\_PATH}}
with the path argument followed by
\textbf{\texttt{LIST\_ADD}}
with value 2 and
\textbf{\texttt{LOAD}}
to the dynamic \texttt{ENGINE}.
If this is not the required behaviour then alternative ctrls can be sent
directly to the dynamic \texttt{ENGINE} using ctrl commands.

The command
\textbf{\texttt{init}}
determines whether to initialize the \texttt{ENGINE}.
If the value is \texttt{0}, the \texttt{ENGINE} will not be initialized.
If it is \texttt{1}, an attempt is made to initialized the \texttt{ENGINE} immediately.
If the
\textbf{\texttt{init}}
command is not present, then an attempt will be made to initialize
the \texttt{ENGINE} after all commands in its section have been processed.

The command
\textbf{\texttt{default\_algorithms}}
sets the default algorithms an \texttt{ENGINE} will supply using the functions
\texttt{ENGINE\_set\_default\_string(3)}.

If the name matches none of the above command names it is assumed
to be a ctrl command which is sent to the \texttt{ENGINE}.
The value of the command is the argument to the ctrl command.
If the value is the string
\textbf{\texttt{EMPTY}},
then no value is sent to the command.

For example:

\begin{ManVerbatim}
[engine_section]
# Configure ENGINE named "foo"
foo = foo_section

[foo_section]
# Load engine from DSO
dynamic_path = /some/path/fooengine.so
# A foo specific ctrl.
some_ctrl = some_value
# Another ctrl that doesn't take a value.
other_ctrl = EMPTY
# Supply all default algorithms
default_algorithms = ALL
\end{ManVerbatim}

\section{FILES}
\label{man:libressl.cnf:files}

\begin{description}
\item[\texttt{/etc/pki/tls/libressl.cnf}]
standard configuration file
\end{description}

\section{EXAMPLES}
\label{man:libressl.cnf:examples}

Here is a sample configuration file using some of the features
mentioned above:

\begin{ManVerbatim}
# This is the default section.
HOME=/temp
RANDFILE= ${ENV::HOME}/.rnd
configdir=$ENV::HOME/config

[ section_one ]
# We are now in section one.

# Quotes permit leading and trailing whitespace
any = " any variable name "

other = A string that can \e
cover several lines \e
by including \e\e characters

message = Hello World\en

[ section_two ]
greeting = $section_one::message
\end{ManVerbatim}

This next example shows how to expand environment variables safely.

Suppose you want a variable called
\textbf{\texttt{tmpfile}}
to refer to a temporary filename.
The directory it is placed in can determined by the
\texttt{TEMP}
or
\texttt{TMP}
environment variables but they may not be set to any value at all.
If you just include the environment variable names and the variable
doesn't exist then this will cause an error when an attempt is made to
load the configuration file.
By making use of the default section both values can be looked up with
\texttt{TEMP}
taking priority and
\texttt{\underline{/tmp}}
used if neither is defined:

\begin{ManVerbatim}
TMP=/tmp
# The above value is used if TMP isn't in the environment
TEMP=$ENV::TMP
# The above value is used if TEMP isn't in the environment
tmpfile=${ENV::TEMP}/tmp.filename
\end{ManVerbatim}

More complex OpenSSL library configuration.
Add \gls{acr:oid}:

\begin{ManVerbatim}
# Default appname: should match "appname" parameter (if any)
# supplied to CONF_modules_load_file et al.
openssl_conf = openssl_conf_section

[openssl_conf_section]
# Configuration module list
alg_section = evp_sect
oid_section = new_oids

[new_oids]
# New OID, just short name
newoid1 = 1.2.3.4.1
# New OID shortname and long name
newoid2 = New OID 2 long name, 1.2.3.4.2
\end{ManVerbatim}

The above examples can be used with any application supporting library
configuration if `\texttt{openssl\_conf}' is modified to match the appropriate
`\texttt{appname}'.

For example if the second sample file above is saved to `\texttt{example.cnf}'
then the command line:

\begin{ManVerbatim}
OPENSSL_CONF=example.cnf openssl asn1parse -genstr OID:1.2.3.4.1
\end{ManVerbatim}

will output:

\begin{ManVerbatim}
0:d=0  hl=2 l=   4 prim: OBJECT            :newoid1
\end{ManVerbatim}

showing that the \gls{acr:oid} `\texttt{newoid1}' has been added as `\texttt{1.2.3.4.1}'.

\section{SEE ALSO}
\label{man:libressl.cnf:see-also}

\begin{itemize}
\item \texttt{libressl(1)}
\item \texttt{CONF\_modules\_load\_file(3)}
\item \texttt{x509v3.cnf(5)} (appendix~\ref{man:x509v3.cnf} on page~\pageref{man:x509v3.cnf})
\end{itemize}

\section{CAVEATS}
\label{man:libressl.cnf:caveats}

If a configuration file attempts to expand a variable that doesn't
exist, then an error is flagged and the file will not load.
This can also happen if an attempt is made to expand an environment
variable that doesn't exist.
For example, in a previous version of OpenSSL the default OpenSSL
master configuration file used the value of
\texttt{HOME}
which may not be defined on non \Unix{} systems and would cause an error.

This can be worked around by including a default section to provide
a default value: then if the environment lookup fails, the default
value will be used instead.
For this to work properly, the default value must be defined earlier
in the configuration file than the expansion.
See the
\texttt{EXAMPLES}
section (section~\ref{man:libressl.cnf:examples} on page~\pageref{man:libressl.cnf:examples})
for an example of how to do this.

If the same variable is defined more than once in the same section,
then all but the last value will be silently ignored.
In certain circumstances such as with \texttt{DN}s, the same field may occur
multiple times.
This is usually worked around by ignoring any characters before an
initial
`\texttt{.}',
for example:

\begin{ManVerbatim}
1.OU="My first OU"
2.OU="My Second OU"
\end{ManVerbatim}

\section{BUGS}
Currently there is no way to include characters using the octal
\texttt{\textbackslash{}\underline{nnn}}
form.
Strings are all \texttt{NUL} terminated, so \texttt{NUL} bytes cannot form part of
the value.

The escaping isn't quite right: if you want to use sequences like
`\texttt{\textbackslash{}n}',
you can't use any quote escaping on the same line.

Files are loaded in a single pass.
This means that a variable expansion will only work if the variables
referenced are defined earlier in the file.

\section{Man Page Notes and Legal}
\label{man:libressl.cnf:legal}

The \LaTeX{} source for this man page started as the man page that shipped with
LibreSSL 2.9.2 and can be revision identified from the following information:

\begin{ManVerbatim}
.\" $OpenBSD: openssl.cnf.5,v 1.5 2019/01/02 07:42:21 jmc Exp $
.\" full merge up to: OpenSSL man5/config b53338cb Feb 28 12:30:28 2017 +0100
.\" selective merge up to: OpenSSL a8c5ed81 Jul 18 13:57:25 2017 -0400
\end{ManVerbatim}

This file was written by Dr. Stephen Henson [steve@openssl.org].
Copyright \copyright{} 1999, 2000, 2004, 2013, 2015, 2016, 2017 The OpenSSL Project.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

\begin{enumerate}
\item Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

\item Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

\item All advertising materials mentioning features or use of this
    software must display the following acknowledgment:
    ``This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit. (\url{https://www.openssl.org/})''

\item The names ``OpenSSL Toolkit'' and ``OpenSSL Project'' must not be used to
    endorse or promote products derived from this software without
    prior written permission. For written permission, please contact
    openssl-core@openssl.org.

\item Products derived from this software may not be called ``OpenSSL''
    nor may ``OpenSSL'' appear in their names without prior written
    permission of the OpenSSL Project.

\item Redistributions of any form whatsoever must retain the following
    acknowledgment:
    ``This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit (\url{https://www.openssl.org/})''
\end{enumerate}

THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
