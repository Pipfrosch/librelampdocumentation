%.TH "HTPASSWD" 1 "2018-07-06" "Apache HTTP Server" "htpasswd"

\chapter{htpasswd -- Manage user files for basic authentication}
\label{man:htpasswd}

\section{SYNOPSIS}
\label{man:htpasswd:synopsis}

\begin{hangparas}{3mm}{1} 
\texttt{\textbf{htpasswd} \mbox{[~-\textbf{c}~]}
                          \mbox{[~-\textbf{i}~]}
                          \mbox{[~-\textbf{m}~|~-\textbf{B}~|~-\textbf{d}~|~-\textbf{s}~|~-\texttt{p}~]}
                          \mbox{[~-\textbf{C}~\underline{cost} ]}
                          \mbox{[~-\textbf{D}~]}
                          \mbox{[~-\textbf{v}~]}
                          \mbox{\underline{passwdfile}}
                          \mbox{\underline{username}}}

\bigskip
\texttt{\textbf{htpasswd} \mbox{-\textbf{b}}
                          \mbox{[~-\textbf{c}~]}
                          \mbox{[~-\textbf{m}~|~-\textbf{B}~|~-\textbf{d}~|~-\textbf{s}~|~-\textbf{p}~]}
                          \mbox{[~-\textbf{C}~\underline{cost}~]}
                          \mbox{[~-\textbf{D}~]}
                          \mbox{[~-\textbf{v}~]}
                          \mbox{\underline{passwdfile}}
                          \mbox{\underline{username}}
                          \mbox{\underline{password}}}

\bigskip
\texttt{\textbf{htpasswd} \mbox{-\textbf{n}}
                          \mbox{[~-\textbf{i}~]}
                          \mbox{[~-\textbf{m}~|~-\textbf{B}~|~-\textbf{d}~|~-\textbf{s}~|~-\textbf{p}~]}
                          \mbox{[~-\textbf{C}~\underline{cost}~]}
                          \mbox{\underline{username}}}


\bigskip
\texttt{\textbf{htpasswd} \mbox{-\textbf{nb}}
                          \mbox{[~-\textbf{m}~|~-\textbf{B}~|~-\textbf{d}~|~-\textbf{s}~|~-\textbf{p}~]}
                          \mbox{[~-\textbf{C}~\underline{cost}~]}
                          \mbox{\underline{username}}
                          \mbox{\underline{password}}}

\end{hangparas}

\bigskip

\section{SUMMARY}
\label{man:htpasswd:summary}
 
\texttt{\textbf{htpasswd}} is used to create and update the flat-files used to store usernames and password for basic authentication of HTTP users.
If \texttt{\textbf{htpasswd}} cannot access a file, such as not being able to write to the output file or not being able to read the file in order to update it, it returns an error status and makes no changes.
 
Resources available from the Apache HTTP server can be restricted to just the users listed in the files created by \texttt{\textbf{htpasswd}}.
This program can only manage usernames and passwords stored in a flat-file.
It can hash and display password information for use in other types of data stores, though.
To use a DBM database see dbmmanage or htdbm.
 
\texttt{\textbf{htpasswd}} hashes passwords using either bcrypt, a version of MD5 modified for Apache, SHA1, or the system's \texttt{\textbf{crypt()}} routine.
Files managed by \texttt{\textbf{htpasswd}} may contain a mixture of different encoding types of passwords; some user records may have bcrypt or MD5-hashed passwords while others in the same file may have passwords hashed with \texttt{\textbf{crypt()}}.
 
This manual page only lists the command line arguments.
For details of the directives necessary to configure user authentication in httpd see the Apache manual, which is part of the Apache distribution or can be found at \url{https://httpd.apache.org/}.
 
\section{OPTIONS}
\label{man:htpasswd:options}
 
\begin{description} 

\item[\texttt{-b}]
Use batch mode; \underline{i.e.}, get the password from the command line rather than prompting for it.
This option should be used with extreme care, since \emph{the password is clearly visible} on the command line.
For script use see the \texttt{\textbf{-i}} option. Available in 2.4.4 and later.  

\item[\texttt{-i}]
Read the password from stdin without verification (for script usage).  

\item[\texttt{-c}]
Create the \texttt{\underline{passwdfile}}.
If \texttt{\underline{passwdfile}} already exists, it is rewritten and truncated.
This option cannot be combined with the \texttt{\textbf{-n}} option.

\item[\texttt{-n}]
Display the results on standard output rather than updating a file.
This is useful for generating password records acceptable to Apache for inclusion in non-text data stores.
This option changes the syntax of the command line, since the \texttt{\underline{passwdfile}} argument (usually the first one) is omitted.
It cannot be combined with the \texttt{\textbf{-c}} option.  

\item[\texttt{-m}]
Use MD5 hashing for passwords.
This is the default (since version 2.2.18).  

\item[\texttt{-B}]
Use bcrypt hashing for passwords.
This is currently considered to be very secure.  

\item[\texttt{-C}]
This flag is only allowed in combination with \texttt{\textbf{-B}} (bcrypt hashing).
It sets the computing time used for the bcrypt algorithm
(higher is more secure but slower, default: 5, valid: 4 to 31).

\item[\texttt{-d}]
Use \texttt{\textbf{crypt()}} hashing for passwords.
This is not supported by the httpd server on Windows and Netware.
This algorithm limits the password length to 8 characters.
This algorithm is \textit{\textbf{insecure}} by today's standards.
It used to be the default algorithm until version 2.2.17.  

\item[\texttt{-s}]
Use SHA hashing for passwords. Facilitates migration from/to Netscape servers using the LDAP Directory Interchange Format (ldif).
This algorithm is \textit{\textbf{insecure}} by today's standards.  

\item[\texttt{-p}]
Use \gls{plaintext} passwords.
Though \texttt{\textbf{htpasswd}} will support creation on all platforms, the httpd daemon will only accept plaintext passwords on Windows and Netware.  

\item[\texttt{-D}]
Delete user.
If the username exists in the specified htpasswd file, it will be deleted.  

\item[\texttt{-v}]
Verify password.
Verify that the given password matches the password of the user stored in the specified htpasswd file.
Available in 2.4.5 and later.  

\item[\texttt{\underline{passwdfile}}]
Name of the file to contain the user name and password.
If \texttt{\textbf{-c}} is given, this file is created if it does not already exist, or rewritten and truncated if it does exist.  

\item[\texttt{\underline{username}}]
The username to create or update in \texttt{\underline{passwdfile}}.
If \texttt{\underline{username}} does not exist in this file, an entry is added.
If it does exist, the password is changed.  

\item[\texttt{\underline{password}}]
The \gls{plaintext} password to be hashed and stored in the file.
Only used with the \texttt{\textbf{-b}} flag.  

\end{description}
 
\section{EXIT STATUS}
\label{man:htpasswd:exit-status}
 
\texttt{\textbf{htpasswd}} returns a zero status ("true") if the username and password have been successfully added or updated in the \texttt{\underline{passwdfile}}.
\texttt{\textbf{htpasswd}} returns \texttt{\textbf{1}} if it encounters some problem accessing files,
\texttt{\textbf{2}} if there was a syntax problem with the command line,
\texttt{\textbf{3}} if the password was entered interactively and the verification entry didn't match,
\texttt{\textbf{4}} if its operation was interrupted,
\texttt{\textbf{5}} if a value is too long (username, filename, password, or final computed record),
\texttt{\textbf{6}} if the username contains illegal characters (see the Restrictions section on page~\pageref{man:htpasswd:restrictions}), and
\texttt{\textbf{7}} if the file is not a valid password file.
 
\section{EXAMPLES}
\label{man:htpasswd:examples}
 
\begin{ManVerbatim}
htpasswd /usr/local/etc/apache/.htpasswd-users jsmith
\end{ManVerbatim}
    
Adds or modifies the password for user \texttt{\textbf{jsmith}}.
The user is prompted for the password.
The password will be hashed using the modified Apache MD5 algorithm.
If the file does not exist, \texttt{\textbf{htpasswd}} will do nothing except return an error.

\bigskip 
\begin{ManVerbatim}
htpasswd -c /home/doe/public_html/.htpasswd jane
\end{ManVerbatim}

Creates a new file and stores a record in it for user \texttt{\textbf{jane}}.
The user is prompted for the password.
If the file exists and cannot be read, or cannot be written, it is not altered and \texttt{\textbf{passwd}} will display a message and return an error status.

\bigskip
\begin{ManVerbatim}
htpasswd -db /usr/web/.htpasswd-all jones Pwd4Steve
\end{ManVerbatim}    

Encrypts the password from the command line (\texttt{\textbf{Pwd4Steve}}) using the \texttt{\textbf{crypt()}} algorithm, and stores it in the specified file.
 
\section{SECURITY CONSIDERATIONS}
\label{man:htpasswd:security-considerations}
 
Web password files such as those managed by \texttt{\textbf{htpasswd}} should \emph{not} be within the Web server's URI space --
that is, they should not be fetchable with a browser.
 
This program is not safe as a \texttt{setuid} executable. Do \emph{not} make it \texttt{setuid}.
 
The use of the \texttt{\textbf{-b}} option is discouraged,
since when it is used the \gls{plaintext} password appears on the command line.
 
When using the \texttt{\textbf{crypt()}} algorithm,
note that only the first 8 characters of the password are used to form the password.
If the supplied password is longer, the extra characters will be silently discarded.
 
The SHA hash format does not use \glsing{salt}: for a given password, there is only one hashed representation.
The \texttt{\textbf{crypt()}} and MD5 formats permute the representation by prepending a random \gls{salt} string,
to make dictionary attacks against the passwords more difficult.
 
The SHA and \texttt{\textbf{crypt()}} formats are insecure by today's standards.
 
\section{RESTRICTIONS}
\label{man:htpasswd:restrictions}
 
On the Windows platform, passwords hasheded with \texttt{\textbf{htpasswd}} are limited to no more than \textbf{255} characters in length.
Longer passwords will be truncated to 255 characters.
 
The MD5 algorithm used by \texttt{\textbf{htpasswd}} is specific to the Apache software;
passwords hashed using it will not be usable with other Web servers.
 
Usernames are limited to \textbf{255} bytes and may not include the character \texttt{\textbf{:}}.

\section{Man Page Notes and Legal}
\label{man:htpasswd:legal}

The \LaTeX{} source for this man page started as the man page that shipped with Apache
httpd 2.4.39 and can be revision identified from the following information:

\begin{ManVerbatim}
.TH "HTPASSWD" 1 "2018-07-06" "Apache HTTP Server" "htpasswd"
\end{ManVerbatim}

The original man page used improper terminology, referring to password hashing as
password encryption. That has been corrected here. See Appendix~\ref{appx:passnotencrypted}
on page~\pageref{appx:passnotencrypted}.

The original man page source file did not contain author or license information, so the
Apache 2.0 license for the Apache httpd project is applicable.
See page~\pageref{license:apache}.
