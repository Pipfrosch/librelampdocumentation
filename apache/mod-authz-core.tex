\chapter{Granularized Authorization Configuration}
\label{chap:apache-core-auth}

First of all it is important to note that with Apache, there is a difference between \emph{Authorization}
and \emph{Authentication}. The latter requires \glspl{authcred} that Apache handles (e.g.\ Basic Authentication,
see chapter~\ref{chap:apache-basicauth} on page~\pageref{chap:apache-basicauth}) while authorization does
not evaluate \glspl{authcred}.

Apache modules that do provide authentication do so with an authorization directive but not all authorization
directives involve authentication.

In subsection~\ref{subsec:apacheconfig:directory:require} on page~\pageref{subsec:apacheconfig:directory:require}
basic use of the \texttt{Require} directive was given.

The Apache module \texttt{mod\_authz\_core} gives us an alternative when we need finer-grained authorization,
such as when we want to allow only certain IP addresses or networks and/or deny certain IP addresses or networks.

Instead of a naked \texttt{Require} directive we can use blocks of \texttt{Require} directives, even nested,
to accomplish what we need to accomplish.

\texttt{mod\_authz\_core} is loaded by default. If for some reason it does not load, you \emph{want} Apache
to fail, so
these blocks should \emph{\underline{not}} be wrapped inside an \texttt{<IfModule mod\_whatever.c>} block.

The three defined blocks:

\begin{description}
\item[\texttt{<RequireAll>}]
  At least one authorization directive within the block must succeed, and none may fail. If none succeed and
  none fail, this directive returns a neutral response.

\item[\texttt{<RequireAny}]
  At least one authorization directive within the block must succeed. If none succeed and none fail, this
  directive returns a neutral response.

\item[\texttt{<RequireNone>}]
  None of the authorization directives within the block may succeed. This block is binary, it never returns
  a neutral response.
\end{description}

These blocks normally are used inside a \texttt{<Directory>} block to control authorization to resources
within that directory.

\section{IP Address and Hostname based Authorization}

IP address and hostname authorization conditions are provided by the \texttt{mod\_authz\_host} Apache
module. This is a standard Apache module you can expect to always be there, you \emph{want} Apache to fail
to start if it is not there and you use its features, so do not put directives that require this
module within an \texttt{<IfModule mod\_whatever.c>} block.

The \texttt{mod\_authz\_host} module adds syntax for four addition options to the \texttt{Require} directive:

\begin{description}

\item[\texttt{Require ip \textit{IP address}}]
Only returns true if there is an IP address match. The IP address can be:

\begin{itemize}
\item A complete IP address (e.g.\ \texttt{192.168.0.12})
\item A partial IP address
(e.g.\ \texttt{192.168})
\item A network with a \gls{netmask} (e.g.\ \texttt{192.168.0.0/255.255.0.0})
\item A network with a \gls{acr:cidr} (e.g.\ \texttt{192.168.0.0/24})
\end{itemize}

\gls{ipv6} addresses \emph{should not} be enclosed in square
brackets when used with this directive.

\item[\texttt{Require host \textit{hostname}}]
Returns true if there is a match by hostname. The hostname can be a complete hostname (e.g.\ \texttt{example.net}) or a partial
hostname beginning with a leading `\texttt{.}'\ (e.g.\ \texttt{.eu}).

This directive relies up \gls{rdns} lookup, making it easy to use to restrict access to specific Internet Service Providers.
For example:

\begin{ShortConfVerbatim}
Require host .ca.comcast.net
\end{ShortConfVerbatim}

That would only return true for users of Comcast Internet Service in California (assuming I am interpreting the \texttt{.ca} correctly).

\item[\texttt{Require forward-dns \textit{hostname}}]
Apache does a forward \gls{acr:dns} query and allows any IP address returned in the query. Because a forward DNS query is used, the
\texttt{hostname} must be complete.

\item[\texttt{Require local}]
Only returns true for the \texttt{localhost}.

\end{description}

\bigskip

\subsection{Possible Attack Vector}

Hostname based authorization relies upon \gls{acr:dns} for security. Do not use either the \texttt{\textbf{host}} or \texttt{\textbf{forward-dns}}
options unless you have a local \gls{acr:dnssec} enforcing recursive resolver, and preferably only use those syntax options if the zones for
the hostnames sign their responses with \gls{acr:dnssec}.

Otherwise your authorization directives can fairly easily be fooled by an attacker.

\section{White Listing}
\label{sec:apache-core-auth:whitelist}

With some resources, such as a web application administrative interface, you may wish to resrict access to
only a limited set of IP addresses and/or hostnames.

To white-list only specific networks and/or IP addresses it is easiest to use a single \texttt{<RequireAny>} block:

\begin{ConfVerbatim}
<RequireAny>
  Require ip 192.168.41.0/24
  Require host .admin.seattle.yourcorp.org
  Require forward-host admin.roving.yourcorp.net
</RequireAny>
\end{ConfVerbatim}

Any IP address that meets the \texttt{192.168.41.0/24} requirement will result in the block validating.

Any IP address that does not meet that requirement but has a \gls{rdns} that is a subdomain of
\texttt{.admin.seattle.yourcorp.org} will result in the block validating.

Furthermore, if the connecting IP addresses matches the list of IP
addresses produced by an \texttt{A}/\texttt{AAAA} record \gls{acr:dns} lookup for the hostname
\texttt{admin.roving.yourcorp.net} then the block will validate.

\section{Blacklisting}

If you find yourself needing to blacklist a particular IP address due to malicous activity, I honestly
recommend doing so with the Linux \texttt{firewalld} daemon.

You also can do it in Apache, but that will not prevent attacks against other ports and it will not
even prevent attacks against Apache resources not covered by the \texttt{<Directory>} block the rule
is applied to.

One way to do it is within a \texttt{<RequireAll>} block:

\begin{ConfVerbatim}
<RequireAll>
  Require all granted
  Require not ip 192.168.99.112
</RequireAll>
\end{ConfVerbatim}

The \texttt{\textbf{not}} keyword is only valid in the context of a \texttt{<RequireAll>} block.

\bigskip
For increased readability, I would instead use a \texttt{<RequireNone>} block inside the
\texttt{<RequireAll>} block:

\begin{ConfVerbatim}
<RequireAll>
  Require all granted
  <RequireNone>
    # Blacklisted for bad behaviour
    Require ip 192.168.99.112
    Require ip 173.255.209.112
    Require ip 2600:3c01::f03c:91ff:fed8:1c0a/64
  </RequireNone>
</RequireAll>
\end{ConfVerbatim}

If any of the IP addresses match what is in the \texttt{<RequireNone>} block, that black will
return false and the \texttt{<RequireAll>} block will fail.

\subsection{Reverse DNS Blacklisting}

It is possible your content is not legal in certain countries. This will not block access in
those countries universally, but it will at least demonstrate an effort to do so.

This can be important. In 2000 a Russian programmer was arrested at a programming conference
in the United States because he wrote and distributed software that was perfectly legal in
Russia but violated the \gls{acr:dmca} in America. Stupid reason to arrest him, but governments
do not always do what is right.

By intentionally blacklisting \gls{rdns} associated with certain countries where you know
your content is not legal, you can at least demonstrate a desire existed not to break their
laws:

\begin{ConfVerbatim}
<RequireAll>
  Require all granted
  <RequireNone>
    # content not legal in these countries
    Require host .cn
    Require host .ir
    Require host .sa
  </RequireNone>
</RequireAll>
\end{ConfVerbatim}

Any IP address that has a \gls{rdns} ending in one of those country codes will be blocked.

Of course not all IP addresses within a country will have a \gls{rdns} that points to a
domain name ending with the country code, and in some countries that may actually be rare,
but it at least is something.

\section{Keep It Short and Simple}

The Apache \texttt{mod\_auth\_core} module allows for very complex and precise rules via
multi-level nesting.

The capability is nice to have and in some cases it \emph{may} be the right solution to
the problem but in some cases it can make the configuration difficult for a human
to grok.

Malicious activity against the server from a particular IP address should be blocked by
the firewall.

White lists for administrative interface are a good thing. Most other needs though can
often be handled by the web application itself, and that is usually the better place to
handle it as it allows loading a web page explaining why they are not allowed access to
the resource.

For public resources I generally avoid using these blocks and just use a single
\mbox{\texttt{Require~all~granted}} directive.

For administrative interfaces I do use a \texttt{<RequireAny>} block, sometimes
in combination with Apache Basic Authentication in which case the \texttt{<RequireAny>}
block is inside a \texttt{<RequireAll>} block that also contains the authentication
directive.

\section{Allow, Deny, Order}

In Apache 2.2 and earlier, the syntax was different and used syntax similar to the following:

\begin{ConfVerbatim}
Order Allow,Deny
Allow from 192.168.33.12
\end{ConfVerbatim}

That syntax still is used in numerous tutorials and is supported if you have the \texttt{mod\_access\_compat}
Apache Module loaded, but it is deprecated and should be avoided. It will not be supported at all in future
versions of Apache.

\section{More Information}

For additional information on selective access, including how to restrict by user agent or how to use
\texttt{mod\_rewrite} to restrict access, see the Apache Manual
\myhref{\apachemanual{}howto/access.html}{Access Control},
\myhref{\apachemanual{}mod/mod\_authz\_core.html}{mod\_authz\_core}, and
\myhref{\apachemanual{}mod/mod\_authz\_host.html}{mod\_authz\_host} documentation.
