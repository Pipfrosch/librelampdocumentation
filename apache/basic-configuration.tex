\chapter{Basic Apache Setup and Configuration}

For the full Apache Manual, see \url{\apachemanual{}}.

This guide is intended to cover setting up a server and some common configuration directives but is
not a complete guide.

\label{chap:apacheconf}

\section{Apache Install}
After making sure your CentOS 7 install is up to date and the LibreLAMP \gls{yum} package repository has been
enabled (see chapter~\ref{chap:install} on page~\pageref{chap:install}), run the following commands:

\begin{TermVerbatim}
[root@host ~]# yum update
[root@host ~]# yum install httpd mod_ssl
\end{TermVerbatim}

The first command will make sure your system is up to date. You can skip that if you have just updated.

The second command will install Apache and the \texttt{mod\_ssl} module
if they are not already installed, along with any dependencies.

Now configure Apache to start at system boot, and manually start it:

\begin{TermVerbatim}
[root@host ~]# systemctl enable httpd.service
[root@host ~]# systemctl start httpd.service
\end{TermVerbatim}

\section{IP Network Note}
\label{sec:apacheconf:ipv6}

It is very common for \glspl{webmaster} to only configure Apache to handle \gls{ipv4} requests.

If your hosting facility does not provide you with an \gls{ipv6} address, your hosting facility is living in the
past. Find a new one. If your hosting facility does provide you with an \gls{ipv6} address, please create the
appropriate \texttt{AAAA} records in your \gls{acr:dns} zone file so that your domain names resolve on the \gls{ipv6}
network and configure Apache virtual hosts that listen on \gls{ipv6} as well as on \gls{ipv4}.

On cellular networks most devices connect through \gls{ipv6}. While they can still retrieve data from the \gls{ipv4}
network, the requests need to go through a \gls{nat64} mechanism to do so. It works, but it is better if it does not have
to.

Please \emph{please} make sure you deploy \gls{ipv6}.

For more information, see the \myhref{https://www.internetsociety.org/deploy360/ipv6/}{Internet Society IPv6} guide.

\section{Webmaster Account}
\label{sec:apacheconf:webmaster}

Every web site should have what I like to call a \gls{webmaster} account. The same webmaster account can be used
for multiple websites hosted on the server, though you can have different webmaster accounts for different websites.

The webmaster account is a \Unix{} user account on the system that has permission to write files in the web site
\gls{docroot} and administer the databases associated with the web site, but it does not have permission to modify
Apache configuration files (other than the \texttt{.htaccess} files with the \gls{docroot}) or modify other system
configuration files.

If the person who is the webmaster for a site is also the system administrator, a different \Unix{} user
account should be used to log in and perform those tasks.

Most webmaster duties do not require a privileged \Unix{} user account, and webmasters tend to log in from
a wide variety of locations using many different devices, increasing the risk that the account they use for
webmaster duties will become compromised.

Hopefully that will never happen to you, but it does happen. When it does happen, it is better if the compromised
account does not even have the ability to become the \texttt{root} user and does not have the ability to use
\texttt{sudo} to do things only the \texttt{root} user should be able to do, like read the private \gls{x509}
cryptography keys or install new \gls{acr:pkix} root trust anchors or reconfigure your \gls{acr:smtp} server to act
as a spam relay.

The webmaster probably should have privileges to administer the databases associated with web sites and generally should have
permission to create and drop databases, those privileges are often needed for the proper administration of web
applications. When updating web applications and the database structure changes, some webmasters will clone the
database to a new one and perform the update using the new one keeping the old database perfectly intact, allowing
them to quickly revert if there is an issue.

The webmaster probably should have \texttt{GRANT} privileges on the database. Creating new database users for each
web application is a very good practice.

If your webmaster is competent, they will not see this separation of privilege as a burden but will appreciate it.

\section{OCSP Stapling}
\label{sec:apacheconf:ocsp}

Historically, Certificate Authorities maintained a \gls{acr:crl} that clients could check to make sure trust in
a \gls{x509} certificate had not been revoked.

That solution did not scale well, the lists became too large so clients would implicitly trust a certificate if
it was not able to estabish a connection to the \gls{acr:crl} server, a frequent exploitable condition.

\myrfc{6960} defines \gls{acr:ocsp} as a solution to this problem. With OCSP, the client does not need to
retrieve a large list of revoked certificates just to verify the certificate it is interested in validating is
not part of that list. When a \gls{acr:ca} supports OCSP, the client can ask about the specific certificate.

To even further mitigate the problem, a server can make the \gls{acr:ocsp} request and cache the result, sending
it to the client along with the handshake. As the OCSP response is signed by the \gls{acr:ca} and short-lived,
the client can have confidence that it is valid.

As the server caches the response, it can send the cached response to every client making a request so those
clients do not need to make individual \gls{acr:ocsp} requests themselves, greatly reducing the load on
the OCSP server. This is called `OCSP Stapling'.

Not all servers support \gls{acr:ocsp} stapling, but Apache does.

By default LibreLAMP enables \gls{acr:ocsp} Stapling on a global level, it applies to every virtual host. However if you
are upgrading an existing Apache install to the Apache that ships with LibreLAMP, it is \emph{possible} that
\gls{acr:rpm} preserved the old configuration and it is still active.

Look at the file \texttt{/etc/httpd/conf.d/ssl.conf} and make sure it contains the following:

\begin{ConfVerbatim}
##
## OCSP Stapling
##

SSLUseStapling On
SSLStaplingCache "shmcb:/run/httpd/ssl_stapling(65536)"
\end{ConfVerbatim}

If it does, you are all set. If it does not, add it but make sure it is added \emph{outside} the context
of the Port 443 default virtual host.

If the old CentOS \texttt{/etc/httpd/conf.d/ssl.conf} is still active, the LibreLAMP modified version with
\gls{acr:ocsp} is probably there too but likely named \texttt{ssl.conf.rpmnew}. If that
is the case, the following will install the default LibreLAMP configuration for that file:

\begin{TermVerbatim}
[root@host ~]# pushd /etc/httpd/conf.d
[root@host conf.d]# cp -p ssl.conf ssl.conf.old.save
[root@host conf.d]# cat ssl.conf.rpmnew > ssl.conf
[root@host conf.d]# systemctl restart httpd.service
\end{TermVerbatim}

Generally, the \texttt{ssl.conf} file should be left alone, \gls{acr:tls} specific changes should be made
in the website specific virtual hosts. Do however make sure that \gls{acr:ocsp} Stapling is enabled globally
in that file.

\section{Create the Document Root}
\label{sec:apacheconf:docroot}

The Apache Daemon serves content for a web site from within what is called the \gls{docroot} for that web site.

When no virtual hosts for a host are defined, the system-wide default Document Root on CentOS
systems is \texttt{/var/www/html} but it is generally considered a good practive to create virtual hosts with their own \gls{docroot} for each
virtual host and to give the site \gls{webmaster} account write permission to the \gls{docroot}.

I generally like to create such directories with the \texttt{/srv} directory and make the Document Root a sub-directory
of a directory named after the domain it is being used for.

For example, if my webmaster user account is \texttt{alice} and the domain is \texttt{example.org} I would create
the Document Root for it using the following commands:

\begin{TermVerbatim}
[root@host ~]# cd /srv
[root@host srv]# mkdir -p example.org/www
[root@host srv]# mkdir example.org/phpinclude
[root@host srv]# chown -R alice:alice example.org
\end{TermVerbatim}

The Document Root for Apache is now created as \texttt{/srv/example.org/www} and the user account \texttt{alice}
has permission to place files in it, as well as permission to create some files that are outside the Document Root.

\gls{acr:php} include files (such as a database connection script that includes \gls{authcred}) can be placed in the
\texttt{/srv/example.org/phpinclude} directory where PHP can be configured to find them but Apache will not
accidentally serve them as text files if the PHP module breaks.

\section{Apache Configuration Format}

An apache configuration file has three basic types of non-white space entries: Comments, Blocks, and Directives.

Comments begin with `\texttt{\#}' hash (sometimes called a pound) character. They exist primarily for the benefit
of humans, Apache ignores comments when it parses a configuration file.

A block starts with an opening tag similar to \gls{acr:xml} nodes and ends with a tag similar to how XML nodes end:

\begin{ConfVerbatim}
<SomeApacheBlock>
# directives here
</SomeApacheBlock>
\end{ConfVerbatim}

Unlike \gls{acr:xml}, however, attributes within the opening tag are not in a \mbox{\texttt{\textit{key="value"}}}
format. Frequently attributes are just a value without a key, and they are not always within straight double-quotes.

When a block is nested within another block, it is only applicable within the scope of the block it is nested in.

The third type of entry is a directive.

A full listing of directives that ship with Apache can be found at \url{\apachemanual{mod/directives.html}}. Other
directives may be available with modules that are not distributed with Apache.

When a directive is inside a block, the directive is only applicable within the scope of the block condition.
When a directive is outside a block, the directive is global in scope.

A directive starts with a \texttt{KeyWord} that identifies the directive followed by one or more parameters or
arguments. The format for the parameters or arguments depends upon the directive.

Most directives only have meaning when the module that defines them is loaded. When the module that defines them
is not loaded, the directive has no meaning and will cause an error when Apache tries to read the configuration file,
resulting in the Apache daemon not starting.

For modules that are a standard part of Apache and expected to be there, this is generally not a big deal. For
modules that are optional however
it is a good idea to put directives defined by those modules into a block that only has scope if the module is
loaded. For example:

\begin{ConfVerbatim}
AddType text/vtt .vtt
AddCharset UTF-8 .vtt
<IfModule mod_brotli.c>
  AddOutputFilterByType BROTLI_COMPRESS text/vtt
  Header append Vary User-Agent env=!dont-vary
</IfModule>
\end{ConfVerbatim}

We do not want to set \texttt{AddOutputFilterByType BROTLI\_COMPRESS} if the module that defines \texttt{BROTLI\_COMPRESS}
is not loaded, so the \texttt{AddOutputFilterByType} directive is placed within the scope of an \texttt{<IfModule>} block
and only has meaning if the conditions of that block are met, namely that the \texttt{mod\_brotli} module is loaded.

When a security flaw is found in that module, or if we do not want it for another reason, we can tell Apache not to load it
and our configuration will not break
as a result.

\section{Virtual Host Configuration}
Apache allows you to configure different responses based upon the \gls{acr:ip} address, Port, and domain name associated
with request. These configurations are called Virtual Hosts. Virtual Hosts are configured within a \texttt{<VirtualHost>}
block.

For the official Apache documentation of virtual hosts, see \url{\apachemanual{vhosts/}}.

What is described here is Name Based Virtual Hosts. There are other types as described in the Apache manual, and many of the
concepts are the same.

Name based virtual hosts will use a \texttt{ServerName} directive. Historically, not all clients supported
the \gls{acr:sni} extension needed to successfully establish a \gls{acr:tls} connection to a name based virtual
host but at this point, support is ubiquitous making IP based virtual hosts no longer necessary for a secure
connection.

A virtual host starts by specifying the \gls{acr:ip} address and port the configuration is applicable to, with the
IP address and port separated by a colon.

\begin{ConfVerbatim}
<VirtualHost 192.168.0.12:80>
# virtual host specific directives
</VirtualHost>
\end{ConfVerbatim}

For \gls{ipv6} addresses, as the address itself includes colons,
the address must be enclosed within \texttt{[]} square brackets.

Directly after the openening \texttt{<VirtualHost>} you can add a \texttt{ServerName} declaration specifying the domain
name the virtual host is applicable to.

\begin{ConfVerbatim}
<VirtualHost 192.168.0.12:80>
ServerName example.org
# virtual host specific directives
</VirtualHost>
\end{ConfVerbatim}

While not strictly required, it is best practice to have a separate file for each domain to contain the virtual host
configurations related to that domain.

For \texttt{example.org} I would name the file \texttt{example.org.conf} for clarity, but what you name it does not
matter too much. As long as it ends with \texttt{.conf} and is located within the \texttt{/etc/httpd/conf.d} directory,
Apache will read it when it starts.

Usually the file will include several virtual hosts (for redirects, \gls{ipv4}, and \gls{ipv6} as well as a few other
directives, such as defining access permission to the \gls{docroot}.

Historically, websites often used a `\texttt{www}' sub-domain for their content. This was due to the web server itself
often running on a different host than the registered domain name. That is now rarely the case.

If you are using the `\texttt{www}' sub-domain (e.g.\ \texttt{www.example.org}) then you should have a virtual host defined
within the configuration file that lacks the `\texttt{www}' sub-domain (e.g.\ \texttt{example.org}) redirect requests to
the version with the sub-domain, but if you are serving the content from the registered domain name itself, then you
should do the opposite.

My \emph{personal} preference is to redirect from the `\texttt{www}' sub-domain to the registered domain name but there
is not a technical reason for this preference.

The LibreLAMP build of Apache follows the Red Hat (and Fedora) tradition of placing these files within the directory
\texttt{/etc/httpd/conf.d/}.

\subsection{Port 80 Redirects}
\label{sec:apacheconf:p80redirect}

If your firewall is blocking Port 80 then you do not strictly need to do this, but it does not hurt to do this anyway.
If your firewall is \emph{not} blocking port 80 then you \emph{must} do this so that requests to the insecure Port 80
get redirected to your secure server.

This section assumes that you are using \texttt{example.org} with requests to \texttt{www.example.org} redirecting
to it.

\begin{ConfVerbatim}
#Port 80 Redirect

<VirtualHost 192.168.0.12:80>
ServerName example.org
Redirect permanent / https://example.org/
</VirtualHost>

<VirtualHost 192.168.0.12:80>
ServerName www.example.org
Redirect permanent / https://example.org/
</VirtualHost>
\end{ConfVerbatim}

Repeat the same two redirect virtual hosts for your \gls{ipv6} address, remembering that Apache requires an \gls{ipv6}
address to be enclosed within \texttt{[]} square brackets.

The \texttt{Redirect permanent} directive causes Apache to send a \gls{http:301} header so that
clients will remember the redirect and automatically apply the redirect the next time they are asked to make a
request to the same domain name without needing to be told.

\subsection{Port 443 Redirects}
\label{sec:apacheconf:p443redirect}

If you want all requests made to \texttt{www.example.org} to redirect to \texttt{example.org} then you need to set
a redirect virtual host for \texttt{www.example.org} on Port 443 as well.

This virtual host will require a few more directives as \gls{acr:tls} is involved.

\begin{ConfVerbatim}
#Port 443 Redirects

<VirtualHost 192.168.0.12:443>
ServerName example.org
Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains"
Redirect permanent / https://example.org/
SSLEngine on
SSLHonorCipherOrder on
SSLCipherSuite "EECDH+CHACHA20 EECDH+AESGCM"
SSLCertificateFile                /full/path/to/certificate/file.crt
SSLCACertificateFile       /full/path/to/certificate-bundle/file.pem
SSLCertificateKeyFile             /full/path/to/private-key/file.key
ErrorLog     logs/example.org.error_log
CustomLog    logs/example.org.access_log combined
</VirtualHost>
\end{ConfVerbatim}

For a description of the \texttt{Strict-Transport-Security} directive on line number 5, please refer to
section~\ref{sec:apachetls:sts} on page~\pageref{sec:apachetls:sts}.

For a description of the \texttt{SSLHonorCipherOrder} and \texttt{SSLCipherSuite} directives on line
numbers 8 and 9, please refer to section~\ref{sec:apachetls:cipher} on page~\pageref{sec:apachetls:cipher}.

For a description of the certificate related directives on lines 10--12, please refer to
section~\ref{sec:apachetls:cert} on page~\pageref{sec:apachetls:cert}.

For a description of the log directives on lines 13 and 14, please refer to section~\ref{sec:apacheconf:logs}
on page~\pageref{sec:apacheconf:logs}.

\section{Live Server Virtual Host}

With the redirects taken care of, it is now time to configure the live host that actually serves data.
For \texttt{example.org} the configuration would look something like this:

\begin{ConfVerbatim}
# Live server

<VirtualHost 192.168.0.12:443>
ServerName example.org
Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains"
DocumentRoot "/srv/example.org/www"
SSLEngine on
SSLHonorCipherOrder on
SSLCipherSuite "EECDH+CHACHA20 EECDH+AESGCM"
SSLCertificateFile                /full/path/to/certificate/file.crt
SSLCACertificateFile       /full/path/to/certificate-bundle/file.pem
SSLCertificateKeyFile             /full/path/to/private-key/file.key
ErrorLog     logs/example.org.error_log
CustomLog    logs/example.org.access_log combined
</VirtualHost>
\end{ConfVerbatim}

The only difference from the Port 443 redirect is that a \texttt{DocumentRoot} directive has been
added while the \texttt{Redirect} directive has been removed.

Again remember to also make a virtual host for your \gls{ipv6} address.

Apache now knows where to find the documents to serve, but it will not serve
them, it will respond with a \gls{http:403} error.

For the server to work, Apache needs to be told who it is allowed to
serve files in the DocumentRoot to. This is accomplished with a
\texttt{<Directory>} directive, as explained in section~\ref{sec:apacheconfig:directory} on page~\pageref{sec:apacheconfig:directory}.

\section{Directory Configuration}
\label{sec:apacheconfig:directory}

Before Apache will serve content from a directory on the local filesystem, Apache has to be configured
to know how it should handle the contents of the directory.

Your \gls{docroot} and any directories referenced in an \texttt{Alias} directive will need to be
configured for access.

Directories containing include files that are not directly served by Apache do not need to be configured
for access.

A \texttt{Directory} configuration is a block configuration:

\begin{ConfVerbatim}
<Directory "/srv/example.org/www">
# Directory specific directives
</Directory>
\end{ConfVerbatim}

This block section belongs \emph{outside} the Virtual Host configuration(s).

When used in the context of a \gls{docroot} I do usually place it in the same configuration file
specific to the domain, but when used in the context of an \texttt{Alias} directive that is applicable to
more than one domain, I usually place it in its own \texttt{whatever.conf} file.

The most commonly used directives are described in the subsections below.

\subsection{The \texttt{Require} Directive}
\label{subsec:apacheconfig:directory:require}

This is the directive you \emph{must} set in the \texttt{<Directory>} configuration for any directory that
Apache is to serve files from. By default, Apache assumes it is not allowed to serve files in the directory.
It needs to be told under what conditions it can allow files from that directory to be served.

There are five basic \texttt{Require} directive options:

\begin{description}
\item[\texttt{Require all granted}]
All requests for files in the directory are granted.

\item[\texttt{Require all denied}]
All requests for files in the directory are rejected.

This is the default behavior but may need to be explicity set if you do not want to allow access to
files in the directory but a parent directory is set to grant access.

\item[\texttt{Require env \textit{env-var [env-var] \ldots{}}}]
Access is granted only if one of the environmental variables is set.

\item[\texttt{Require method \textit{http-method [http-method] \ldots{}}}]
Access is granted but only for the specified \glspl{http-method}.

\item[\texttt{Require expr \textit{expression}}]
Access is granted only if the \texttt{\textit{expression}} evaluates to \texttt{TRUE}.

\end{description}

\bigskip

Some Apache modules extend the syntax adding additional parameters.

For a public web server, \texttt{Require all granted} is probably what you want.

Also see chapter~\ref{chap:apache-core-auth} on page~\pageref{chap:apache-core-auth}.

\subsection{The \texttt{AllowOverride} Directive}

This directive specifies whether or not a \texttt{.htaccess} file is allowed to override directives
within the \texttt{<Directory>} configuration.

The directive allows for the following arguements:

\begin{description}

\item[\texttt{None}]
The ability to use a \texttt{.htaccess} file is not available. The file if present is ignored.
This is the default.

\item[\texttt{All}]
Any diretive that is legal in a \texttt{.htaccess} file is allowed.

\item[\texttt{AuthConfig}]
Any authorization directives in the \texttt{.htaccess} is allowed.

\item[\texttt{FileInfo}]
Any directive controlling document types in the \texttt{.htaccess} is allowed.

\item[\texttt{Indexes}]
Any directives controlling directory indexing in the \texttt{.htaccess} file is allowed.

\item[\texttt{Limit}]
Any directives that control host access in the \texttt{.htaccess} file is allowed.

\item[\texttt{Nonfatal=[Override\\|Unknown\\|All]}]
Allow the use of AllowOverride options to treat errors in the \texttt{.htaccess} file as non-fatal.

  \begin{description}
  \item[\texttt{Nonfatal=Override}]
  Directives forbidden by \texttt{AllowOverride} are treated as nonfatal.

  \item[\texttt{Nonfatal=Unknown}]
  Treats unknown directives as nonfatal. This includes typos and directives that only have meaning when
  a module is loaded that is not currently loaded.

  \item[\texttt{Nonfatal=All}]
  Applies both of the above.
  \end{description}

\item[\texttt{Options[=\textit{Option},\ldots]}]
  Allows the use of directives that control specific directory features. When an `\texttt{=}' sign is present,
  it is followed by a `\texttt{,}' (comma) delimited list of options that can be set in the \texttt{.htaccess} file.
\end{description}

\bigskip

Generally speaking, if you plan to allow the use of a \texttt{.htaccess} file at all, it is easiest to just
set \mbox{\texttt{AllowOverride~All}}.

Changes to the Apache configuration file require restarting the server daemon. Changes to a \texttt{.htaccess}
file do not.

Technically it is more secure to limit what can be set in the \texttt{.htaccess} file and if you are both the
\gls{webmaster} and the system administrator, that is better. However if your webmaster is someone else,
it is quite frustrating for the webmaster to need to put in a support ticket to get this directive changed
so they can set a \texttt{.htaccess} directive they need to set to do their job.

For web applications with an admin interface, I do recommend using the \mbox{\texttt{AllowOverride~None}}
directive for the directory with the admin interface and placing all needed directives within the Apache
\texttt{<Directory>} directive for that directory, admin interfaces warrant the additional security.

\subsection{The \texttt{DirectoryIndex} Directive}

This directive is not required to be set within the context of a \texttt{Directory} configuration, it
can be set globally or in a Virtual Host as well.

When it is set withing the context of a \texttt{Directoty} configuration, it will use what is set
in the Directory configuration and only fall back to what is set globally or in a Virtual Host if
there is not a match.

This directive sets the file name(s) Apache looks for when a directory is requested rather than when a
specific file is requested.

The default in LibreLAMP is defined in the file \texttt{/etc/httpd/conf/httpd.conf} as
\begin{ConfVerbatim}
DirectoryIndex index.html
\end{ConfVerbatim}

If you have \gls{acr:php} installed, it also sets this directive in the file \texttt{/etc/httpd/conf.d/php.conf}:
\begin{ConfVerbatim}
DirectoryIndex index.php
\end{ConfVerbatim}

This effectively means that by default, Apache in LibreLAMP will look for the \texttt{index.php} file
first (assuming you have \gls{acr:php} installed) and if not found, it will then look for a \texttt{index.html} file to serve.

Generally speaking, you do not need to set this directive unless you want the default file served
when a directory is requested by the client to be something else.

\subsection{The \texttt{Options} Directive}

This directive is used to describe what additional features are applicable to a particular directory:

\begin{description}

\item[\texttt{None}]
Do not apply any options to this directory, even if configured in a parent directory.

\item[\texttt{All}]
Configure the Apache server to use all options \emph{except} for \texttt{MultiViews}.

\item[\texttt{ExecCGI}]
If you have \texttt{mod\_cgi} enabled, allow execution of \gls{acr:cgi} scripts in this directory.

\item[\texttt{FollowSymlinks}]
If the requested file is a symbolic link \emph{and} Apache is configured to allow serving the file
it points to, allow serving the file.

\item[\texttt{Includes}]
Allow for \gls{ssi} using the \texttt{mod\_include} Apache module.

\item[\texttt{IncludesNOEXEC}]
Allow for \gls{ssi} but do not allow \texttt{\#exec cmd} or \texttt{\#exec cgi}.

\item[\texttt{Indexes}]
When the request is made to a directory but a default index file (defined by \texttt{DirectoryIndex}) does
not exist in the directory, then \texttt{mod\_autoindex} generates a formatted listed of the directory.

\item[\texttt{MultiViews}]
Allow content-negotiated alternate views based upon client-supplied preferences (such as preferred language).
To understand this option, see the \myhref{\apachemanual{content-negotiation.html}}{\texttt{mod\_negotiation}}
documentation.

Most users do not need this option enabled but it is very useful when the directory contains static content that has
been translated to other languages, e.g.\ a static privacy policy page in English, French, and Spanish.

\item[\texttt{SymlinksIfOwnerMatch}]
Similar to the \texttt{FollowSymlinks} option but restricts it to requiring the \Unix{} file owner of
the symbolic link match the \Unix{} file owner of the file it links to.

This is primarily needed for security when many different users have web sites on the same host.
\end{description}

\bigskip
\noindent{}The \texttt{Options} directive uses the following syntax:

\begin{ConfVerbatim}
Options value1 value2 value3
\end{ConfVerbatim}

When the \texttt{valueN} options are not preceded by a `\texttt{+}' or `\texttt{-}' operator, the
\texttt{Options} directives overrides any \texttt{Options} directive that may be applicable to a
parent directory.

When `\texttt{+}' or `\texttt{-}' operators are present, then the specified values are added to or
substracted to values set to an applicable parent directory.

As such, you can not mix values that do not have an operator with values that do not.

\subsubsection{Example Usage}

\begin{ConfVerbatim}
<Directory "/srv/example.org/www">
  Require all granted
  Options Includes Indexes
</Directory>

<Directory "/srv/www/example.org/www/cgi-bin">
  DirectoryIndex index.cgi
  Options -Indexes +ExecCGI
</Directory>
\end{ConfVerbatim}

The directory \texttt{/srv/example.org/www} will allow \gls{ssi} and allow indexes to be generated for itself
and all sub-directories.

However in the directory \texttt{/srv/example.org/www/cgi-bin} \gls{ssi} is still allowed, but the generation
of indexes has been revoked and the added option of allowing \gls{acr:cgi} scripts via \texttt{mod\_cgi} has
been added.

Please not that this is just an example, ordinarily a \texttt{cgi-bin} directory would not be a sub-directory
of the \gls{docroot} but would be \emph{outside} the document root and use an \texttt{Alias} directive,
as discussed in section~\ref{sec:apacheconfig:alias} on page~\pageref{sec:apacheconfig:alias}.

\subsection{Working Example}

Here is a basic working example of a \texttt{<Directory>} configuration:

\begin{ConfVerbatim}
<Directory "/srv/example/www">
  Require all granted
  Options SymlinksIfOwnerMatch Indexes
  AllowOverride All
</Directory>
\end{ConfVerbatim}

Apache will serve any requests to files within that directory.
Apache will follow symbolic links in the directory but only if the file the symbolic link points to has the
same \Unix{} owner as the \Unix{} owner of the symbolic link.
Apache will generate an index of the directory (or sub-directories) if a request is made to the directory
and an index file is not found.
Apache will apply any directive found within a \texttt{.htaccess} file that is legal in a \texttt{.htaccess}
file, assuming the needed module for the directive is loaded.

