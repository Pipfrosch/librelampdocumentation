\chapter{Apache Basic Authentication}
\label{chap:apache-basicauth}

The \glsfirst{acr:http} provides for user authentication at the protocol level, defined in
\myrfc{7235}. Apache implements this with the \texttt{mod\_auth\_basic} module.

Most web applications that use authentication handle the authentication themselves and do
not make use of \myrfc{7235}. When done properly, that is actually a good thing.

However Apache Basic Authentication still has value. My personal use cases:

\begin{itemize}
\item Utility servers running utility web applications I wrote, e.g.\ the generation of
\gls{acr:dns} zone files from a database. These utility web applications are not public and
do not have a built-in
authentication system. They are protected by a combination of IP address white-listing
(see section~\ref{sec:apache-core-auth:whitelist} on page~\pageref{sec:apache-core-auth:whitelist})
and Apache Basic Authentication. The scripts that need to access them can authenticate with
\gls{acr:http} authentication a lot easier than scripting them to fill out a form for web
application controlled authentication.

\item Web Application Admin Interface. Those who need to log into the admin interface to
a web application are given a common login and password for Apache Basic Authentication.
This protects the admin interface login page from
from brute force bot attacks, so if one of the admin users has set a weak password for their
account within the web application, bots do not even get that far because they can not access
the admin account login without first authenticating with Apache Basic Authentication.
\end{itemize}

Make \emph{damn sure} your server only allows access using \gls{acr:tls}, \gls{authcred}
should \emph{never} be sent via \gls{plaintext}.

\section{The Password File}

Apache Basic Authentication uses a file to store hashed passwords. This file should be
\emph{outside} the webroot in a directory only the \gls{webmaster} and the Apache daemon
have permission to read.

If \texttt{/srv/example.org/www} is our \gls{docroot} and \texttt{alice} is our
\gls{webmaster}:

\begin{TermVerbatim}
[root@host ~]# cd /srv/example.org
[root@host example.org]# mkdir basicAuth
[root@host example.org]# chmod 750 basicAuth
[root@host example.org]# chown alice:apache basicAuth
\end{TermVerbatim}
%stopzone

\emph{Please note in some \Unix{} distributions, the Apache daemon does not run using the group \texttt{apache}}.

\bigskip
The password file can be created and managed with the \gls{webmaster} account, the only reason for using the
\texttt{root} account above was for the \texttt{chown} command to asign group ownership to \texttt{apache}.

The password file is managed using the \texttt{htpasswd} utility. The man page for that utility can be viewed
in appendix~\ref{man:htpasswd} on page~\pageref{man:htpasswd}.

There are several hash algorithms to choose from. Many of them are no longer considered safe.

\subsection{Hash Algorithm}
According to the man page, both their customized \gls{md5} and the \gls{bcrypt} algorithm are safe. In upstream
Apache their customized MD5 is the default. However even their customized MD5 is not very resistant to brute-force
dictionary attacks, so \texttt{htpasswd} in LibreLAMP has been patched to use bcrypt as the default hash algorithm.

I highly recommend only using \gls{bcrypt} with Apache Basic Authentication until additional
safe \glspl{passhash} are added.

With \gls{bcrypt} the cost flag (the \texttt{-C} switch) specifies the computing
time needed to hash the password. It takes an integer argument between
4 and 31 inclusive, with 12 being the LibreLAMP default if not specified.

The cost factor
determines how many rounds of hashing is performed as a power of two. A cost factor of 12 means
$2^{12} = 4096$ hash rounds are performed. A cost factor of 13 would double that as $2^{13} = 8192$.

I tend to set the cost factor to 15 (32,768 rounds). Do not use a value below 12.

\subsection{Create the Password File}

When creating a new file, the \texttt{-c} switch is used. The location for the file is specified after all the
switches with the login name of the initial user added after the file path.

To create a file called `\texttt{/srv/example.org/basicAuth/passhash}' for a login user named `\texttt{phoenix}'
with a cost factor of \texttt{15}:

\begin{TermVerbatim}
[alice@host example.org]$ htpasswd -C 15 -c /srv/example.org/basicAuth/passhash phoenix
New password: 
Re-type new password: 
Adding password for user phoenix
\end{TermVerbatim}
%stopzone

Note that in the example, `\texttt{New password:}' and `\texttt{Re-type new password:}' are prompts from the program.

\bigskip
This is what the contents of the file looks like:

\begin{ConfVerbatim}
phoenix:$2y$15$TGI2BExd4ieUzIuwFo4ycuKEIc13iN.dD6659uFs2xNU1tyrtmGoW
\end{ConfVerbatim}
%stopzone

\subsubsection{Adding Additional Users}

Adding additional users is done the same as creating the file, only lacking the \texttt{-c} switch:

\begin{TermVerbatim}
[alice@host example.org]$ htpasswd -C 15 /srv/example.org/basicAuth/passhash robert
New password: 
Re-type new password: 
Adding password for user robert
\end{TermVerbatim}
%stopzone

\subsubsection{Deleting Existing Users}

Existing users can be deleted using the \texttt{-D} switch:

\begin{TermVerbatim}
[alice@localhost example.org]$ htpasswd -D /srv/example.org/basicAuth/passhash tom
Deleting password for user tom
\end{TermVerbatim}
%stopzone

\subsection{Apache Directives}
Configuration directives for Apache Basic Authentication can be placed with a \texttt{<Directory>} block, a
\mbox{\texttt{<Location>}} block, a \texttt{<Files>} block, or within a \texttt{.htaccess} file if the
directory the file is in has an applicable \texttt{AllowOverride} directive that allows \texttt{AuthConfig}.

My personal preference is to put Apache Basic Authentication directives into a \texttt{<Directory>}
block. These are the directives needed:

\begin{description}
\item[foo]
bar

\end{description}

\subsection{Security Issues}

The \texttt{htpasswd} command does not verify the quality of the password being used. Do not allow users
to set their own passwords without an intermediary interface that validates the quality of the password.
The \gls{bcrypt} implementation is standard, you can create a web interface that checks the quality of
the password and then manually (or \emph{very carefully} automate) add the resulting username and hash
to the file.

The default cost for \gls{bcrypt} is compiled into the \texttt{htpasswd} binary. As time passes, computing
power increases, and the default value should be increased.
According to \myhref{https://simple.wikipedia.org/wiki/Moore\%27s\_law}{Moore's Laws}, computing power
doubles about every two years. When bcrypt was released, it had a minimum cost of four. A cost
increase of one doubles the rounds, so $4 + (2019 - 1999)/2 = 14$ which is actually higher than the
default cost of twelve rounds in the patched version of \texttt{htpasswd} that LibreSSL currently ships with,
but the pace of computing power increase has slowed somewhat in the last ten years.

Unfortunately code maintainers do not often keep the default cost up to date. I updated the minimum to twelve in
LibreLAMP, it is still set to five in vanilla Apache 2.4.39 which is \emph{way} too small.
Will I remember to update it in LibreLAMP from
time to time? I hope so, but unfortunately you can not just update a configuration file if I forget to.

In web applications, the cost factor can automatically be increased from time to time for a given password
without requiring the user to set a new password as long as the user logs in. That capability does not
exist with Apache Basic Authentication.
It is \emph{possible} to write a script that upgrades existing entries to add additional rounds based
on the current hash and number of rounds, but \texttt{htpasswd} does not have that capability natively.
