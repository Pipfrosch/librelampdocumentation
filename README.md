LibreLAMP Documentation Project
===============================

The purpose of this project is to produce printable beautiful documentation
for the software stack at https://librelamp.com/

Documentation is being written in [\LaTeX{}](https://tug.org/texlive/).

I use TeXLive 2016 as I only update my major version of TeXLive when I have a
need to, but what is here should compile with newer versions of TeXLive as
well. I hope.

When finished, the actual PDF file will be hosted elsewhere, it does not make
sense to track the PDF in git.

The script `buildDoc.sh` builds the PDF file, assuming there are no errors in
the \LaTeX{} and you have `pdflatex` and `makeglossaries` in your UNIX shell
path.

With no arguments or an argument it does not understand, it builds
`Documentation.pdf` which is one-sided with color. This is best for screen
display.

With the argument `twoside` it builds `DocumentationTwoSided.pdf` which is
two-sided. If you want to print and bind and want color, this is the best
option.

With the argument `mono` it builds `DocumentationMono.pdf` which is one-sided
with black, white, and light-gray. This is best for printing without color ink
if you can not print two-sided.

With the argument `twisidemono` it builds `DocumentationTwoSidedMono.pdf`
which is two-sided. This is best for when you do not want to use color ink
but do want to print two-sided for binding.

With the argument `all` it builds all four versions of the pdf.

If you have the [MathTime Pro 2 Fonts](https://www.pctex.com/mtpro2.html) package
where \LaTeX{} can find it, then the `times` package will be used as the main
document font with MathTime Pro 2 used for any maths environments. Otherwise
the `lmodern` package will be used for both the main document font and any
maths environment.

This is necessary so that inline maths environment matches the body font it
is used within.

