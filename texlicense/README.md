LICENSE FILES
=============

The files in this directory are `\LaTeX{}` source files for licenses included in
LibreLAMP.

Their content should not be changed, other than typesetting changes.

The various GNU licenses started as `\LaTeX{}` files, some editing of the
typesetting was done.

The other files started as plain text files. `\LaTeX{}` typesetting commands was
added to them.

These files should be considered *Invariant* as defined in the GNU FDL 3.0.

They must be included without alteration (except for typesetting needs).
