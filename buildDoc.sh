#!/bin/bash
ARG="$1"
NORM=0
MONO=0
DOUBLENORM=0
DOUBLEMONO=0

function clean {
  rm -f *.acn
  rm -f *.acr
  rm -f *.alg
  rm -f *.aux
  rm -f *.crg
  rm -f *.cro
  rm -f *.crs
  rm -f *.htg
  rm -f *.hto
  rm -f *.hts
  rm -f *.glsdefs
  rm -f *.ist
  rm -f *.listing
  rm -f *.log
  rm -f *.mgg
  rm -f *.mgo
  rm -f *.mgs
  rm -f *.out
  rm -f *.toc
}

case "${ARG}" in
  clean)
    clean
    ;;
  mono)
    MONO=1
    ;;
  twoside)
    DOUBLENORM=1
    ;;
  twosidemono)
    DOUBLEMONO=1
    ;;
  all)
    NORM=1
    MONO=1
    DOUBLENORM=1
    DOUBLEMONO=1
    ;;
  *)
    NORM=1
    ;;
esac

# make the master files
sed -e s?"^\\\colortrue"?"\\\colorfalse"? < Documentation.tex > DocumentationMono.tex
sed -e s?"^\\\twosidefalse"?"\\\twosidetrue"? < Documentation.tex > DocumentationTwoSided.tex
sed -e s?"^\\\colortrue"?"\\\colorfalse"? < DocumentationTwoSided.tex > DocumentationTwoSidedMono.tex

if [ ${NORM} -eq 1 ]; then
clean
pdflatex Documentation.tex && makeglossaries Documentation && pdflatex Documentation.tex && pdflatex Documentation.tex > build.log 2>&1
fi

if [ ${MONO} -eq 1 ]; then
clean
pdflatex DocumentationMono.tex && makeglossaries DocumentationMono && pdflatex DocumentationMono.tex && pdflatex DocumentationMono.tex
fi

if [ ${DOUBLENORM} -eq 1 ]; then
clean
pdflatex DocumentationTwoSided.tex && makeglossaries DocumentationTwoSided && pdflatex DocumentationTwoSided.tex && pdflatex DocumentationTwoSided.tex
fi

if [ ${DOUBLEMONO} -eq 1 ]; then
clean
pdflatex DocumentationTwoSidedMono.tex && makeglossaries DocumentationTwoSidedMono && pdflatex DocumentationTwoSidedMono.tex && pdflatex DocumentationTwoSidedMono.tex
fi

exit 0
