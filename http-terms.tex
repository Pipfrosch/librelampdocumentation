% general HHTP glossary
\longnewglossaryentry{contentnegotiation}
{
  name=content negotiation,
  type=mymain
}
 {An \gls{acr:http} mechanism by which the client can request a specific version of the
 resource. This is usually \emph{though not exclusively} used in conjunction with the
 preferred language of the client.

 For example, a client that prefers documents in Thai will specify that as the preferred
 language to the server. The server may serve the document in German by default, but if
 it has a Thai version and the client prefers Thai, then the server can send the Thai
 version of the document instead.

 Other examples, a document may use \gls{acr:png} and \gls{acr:jpeg} images but if the
 client advertises that it supports \gls{acr:webp} then the server may send a version of
 the document that references WebP images instead.}

 
\longnewglossaryentry{http-instance}
{
  name=instance (HTTP),
  type=mymain,
  plural=instances (HTTP)
}
  {Defined in \myrfc{3230} as ``The entity that would be returned in a [\gls{http:200}]
  response to a \gls{GET} request, at the current time, for
  the selected variant of the specified resource,
  with the application of zero or more content-codings,
  but without the application of any
  \glspl{http-im} or transfer-codings.''}

\longnewglossaryentry{http-im}
{
  name=instance manipulation,
  type=mymain
}
  {Defined in \myrfc{3230} as ``An operation on one or more [\glspl{http-instance}] which may
  result in an instance being conveyed from server to
  client in parts, or in more than one response
  message.  For example, a range selection or a delta
  encoding.  Instance manipulations are end-to-end,
  and often involve the use of a cache at the client.''}

\longnewglossaryentry{ssi}
{
  name=Server-Side Includes,
  type=mymain
}
  {A scripting language used mostly by web servers that allows on the fly creation of web pages
  to be served by including files with partial content.

  Largely super-ceded by \gls{acr:php} and other more advanced technologies.}

\longnewglossaryentry{webdav}
{
  name=WebDAV,
  type=mymain
}
  {An implementation of \gls{acr:dav} over \gls{acr:http}. See \myrfc{4918}.}

% http methods
\longnewglossaryentry{http-method}
{
  name=HTTP request method,
  type=mymain
}
  {The part of the \gls{acr:http} request from a client that specifies the desired action
  to the server. See \myrfc{7231} section 4.

  The currently recognized methods are \gls{GET}, \gls{HEAD}, \gls{POST}, \gls{PUT},
  DELETE, CONNECT, OPTIONS, TRACE, and PATCH.}

\longnewglossaryentry{GET}
{
  name=GET (HTTP),
  type=mymain
}
  {An \gls{http-method} where all optional paremeters (if any) for the request are sent as part
  of the \gls{acr:uri} request in the query string.

  This method is only suitable for retrieving response data.}

\longnewglossaryentry{HEAD}
{
  name=HEAD (HTTP),
  type=mymain
}
  {An \gls{http-method} that asks for a response identical to what would be retrieved with the
  \gls{GET} method, but without the response body.}

\longnewglossaryentry{POST}
{
  name=POST (HTTP),
  type=mymain
}
  {An \gls{http-method} where data is sent in the body of the request. Usually
  used with forms and Ajax request.}

\longnewglossaryentry{PUT}
{
  name=PUT (HTTP),
  type=mymain
}
  {An \gls{http-method} that replaces all current representations of the target
  resource with the request payload.}

% need PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH

% http response codes
\longnewglossaryentry{http:informational}
{
  name=1XX Informational response,
  type=httpstatus,
  sort=099x
}
  {\gls{acr:http} response codes that indicate the request has been received and understood
  by the server.}

\longnewglossaryentry{http:100}
{
  name=100 Continue,
  type=httpstatus
}
  {The request headers have been received, the client should send the request body.

  When clients wish to send a large content body, the \emph{should} initially just send the
  request headers including a \texttt{Expect: 100-continue} header. If the server is
  willing to accept the request, it will respond with this header and the client may
  continue to send the request body.}

\longnewglossaryentry{http:101}
{
  name=101 Switching Protocols,
  type=httpstatus
}
  {The client has sent a request to switch protocols and the server is capable and willing
  to do so.}

\longnewglossaryentry{http:102}
{
  name=102 Processing,
  type=httpstatus
}
  {\gls{webdav} requests may require file system operations that take some time to complete.
  This header lets the client the request has been received and is being processed but is
  not yet complete.

  Sending this header prevents the client from assuming the connection has timed out and
  wasting resources by attempting the request again.

  See \myrfc{2518}.}

\longnewglossaryentry{http:103}
{
  name=103 Early Hints,
  type=httpstatus
}
  {This is used to send some response headers before the final \gls{acr:http} message is sent.

  See \myrfc{8297}.}

\longnewglossaryentry{http:success}
{
  name=2XX Success response,
  type=httpstatus,
  sort=199x
}
  {\gls{acr:http} response codes that indicate the request has been received and understood
  and successfully acted upon.}

\longnewglossaryentry{http:200}
{
  name=200 OK,
  type=httpstatus
}
  {The request is successfully handled by the server.

  This is the response most commonly sent when the request is good and everything
  is working as it should.}

\longnewglossaryentry{http:201}
{
  name=201 Created,
  type=httpstatus
}
  {The request to create a new resource has been completed.}

\longnewglossaryentry{http:202}
{
  name=202 Accepted,
  type=httpstatus
}
  {The server has accepted the request but it has not yet processed the request.}

\longnewglossaryentry{http:203}
{
  name=203 Non-Authoritative Information,
  type=httpstatus
}
  {The server is a proxy that modifies content. It received a \gls{http:200} from the
  origin and is returning a modified version of that response.

  This is mostly used for accelerator proxies, e.g.\ services that compress images
  and other resources reducing the bandwidth needed by the requesting client.}

\longnewglossaryentry{http:204}
{
  name=204 No Content,
  type=httpstatus
}
  {The request was successfully processed but the server is not returning any content.}

\longnewglossaryentry{http:205}
{
  name=205 Reset Content,
  type=httpstatus
}
  {The request was successfully processed but no content is being sent. The client
  needs to reset the document view.}

\longnewglossaryentry{http:206}
{
  name=206 Partial Content,
  type=httpstatus
}
  {The request was successfully processed but the response only includes part of the
  content.

  This is used when the client specifies a byte range and is usually used when resuming
  a download or with multimedia files that are large.

  See \myrfc{7233}.}

\longnewglossaryentry{http:207}
{
  name=207 Multi-Status,
  type=httpstatus
}
  {Used with \gls{webdav}. The message body is usually \gls{acr:xml} and can contain
  separate response codes.

  See \myrfc{4918}.}

\longnewglossaryentry{http:208}
{
  name=208 Already Reported,
  type=httpstatus
}
  {Used with \gls{webdav}. Members of a \gls{acr:dav} binding have already been
  enumerated and are not being included again.

  See \myrfc{5842}.}

\longnewglossaryentry{http:226}
{
  name=226 IM Used,
  type=httpstatus
}
  {The request was successful and the response represents one or more
  \glspl{http-im} to be applied to the current \gls{http-instance}.

  See \myrfc{3229}.}

\longnewglossaryentry{http:redirect}
{
  name=3XX Redirect response,
  type=httpstatus,
  sort=299x
}
  {\gls{acr:http} response codes that directs the client to take additional action to complete the request.}

\longnewglossaryentry{http:300}
{
  name=300 Multiple Choices,
  type=httpstatus
}
  {A redirect but with multiple choices for the client to choose from.

  This is sometimes done to allow the client to select a preferred multimedia format,
  such as \gls{acr:aac} or \gls{acr:mp3}.}

\longnewglossaryentry{http:301}
{
  name=301 Moved Permanently,
  type=httpstatus
}
  {A redirect telling the client the resource has been moved permanently.

  The client may cache the \gls{acr:url} so it can apply the redirect in the future
  without making the request, and the client may update bookmarks that point to the
  old URL so that they now point to the redirect URL.}

\longnewglossaryentry{http:302}
{
  name=302 Found,
  type=httpstatus
}
  {The behavior of browsers with this response code is not consistent. It should be
  avoided.

  Originally it was intended to have the behavior of \gls{http:307} but browsers
  did not implement it correctly. \gls{http:303} was added to give the type of
  functionallity browsers implemented, and \gls{http:307} was added to give the
  original function of 302 but with a new code.

  Most browsers treat a 302 response code as if it were 303.}

\longnewglossaryentry{http:303}
{
  name=303 See Other,
  type=httpstatus
}
  {The response the client seeks can be found at the specified \gls{acr:url}.}

\longnewglossaryentry{http:304}
{
  name=304 Not Modified,
  type=httpstatus
}
  {Used to tell a client that the response has not changed since the version the
  client specified in its request header, it should use that response.

  This is used when a client has cached a response but the client is not sure
  if the cached version it has is current. The client will send a new request
  for the resource including information about the version of the file it has.
  If the version it has is current, the server sends this header and the client
  does not need to receive new data. If the version is not current, then the
  server will either send the updated response data or a different header such
  as \gls{http:404} if the resource does not exist.

  See \myrfc{7232}.}

\longnewglossaryentry{http:305}
{
  name=305 Use Proxy,
  type=httpstatus
}
  {A redirect to a proxy to retrieve the resource from.

  Due to security concerns, many browsers do not implement the behavior this
  status code is intended for.}

\longnewglossaryentry{http:306}
{
  name=306 Switch Proxy,
  type=httpstatus
}
  {Deprecated. The response is fulfilled, but future requests for the resource
  should go through the specified proxy.

  This status code is no longer implemented by clients.}

\longnewglossaryentry{http:307}
{
  name=307 Temporary Redirect,
  type=httpstatus
}
  {The resource is temporarily located at a different location. Use that location
  to retrieve it, but do not cache the redirect or update bookmarks.}

\longnewglossaryentry{http:308}
{
  name=308 Permanent Redirect,
  type=httpstatus
}
  {Very similar to \gls{http:301} except the method can not change. So if a 308
  response is sent for a \gls{POST} form request, the client will use POST with
  the redirect \gls{acr:url}.

  See \myrfc{7538}.}

\longnewglossaryentry{http:client-error}
{
  name=4XX Client error response,
  type=httpstatus,
  sort=399x
}
  {\gls{acr:http} response codes that indicate the server can not complete the request due to a
  bad client request.}

\longnewglossaryentry{http:400}
{
  name=400 Bad Request,
  type=httpstatus
}
  {The request was not processed because it contains an error.}

\longnewglossaryentry{http:401}
{
  name=401 Unauthorized,
  type=httpstatus
}
  {The request can not be processed due to failed or lacking \glspl{authcred}.

  This differs from a \gls{http:403} header in that it is specifically for
  requests where \glspl{authcred} are sent performed using Basic Access Authentication
  or Digest Access Authentication.

  See \myrfc{7235}.}

\longnewglossaryentry{http:402}
{
  name=402 Payment Required,
  type=httpstatus
}
  {Client behavior for this response code is not yell defined and I doubt it
  ever will be, the \gls{acr:http} protocol is the wrong place for handling
  billing issues.

  I believe the original intent was for a digital cash system allowing
  payments to be made through the browser software itself to access a resource
  but securing such a system is just not feasible.

  Some websites do use the response code, but most just redirect to a web page
  where the customer can pay for access to the resource.}

\longnewglossaryentry{http:403}
{
  name=403 Forbidden,
  type=httpstatus
}
  {The request was successfully received but the server is refusing to respond.

  This response is usually sent when the resquest is for a protected resource but
  the client did not send validating \gls{authcred}.}

\longnewglossaryentry{http:404}
{
  name=404 Not Found,
  type=httpstatus
}
  {The requested resource was not found by the server. It may be available in the
  future.}

\longnewglossaryentry{http:405}
{
  name=405 Method Not Allowed,
  type=httpstatus
}
  {The requested \gls{http-method} is not allowed for the requested resource.

  An example would be trying to use \gls{POST} on a resource that only allows
  \gls{GET}.}

\longnewglossaryentry{http:406}
{
  name=406 Not Acceptable,
  type=httpstatus
}
  {The resource requested can not be served in compliance with the specified Accept headers sent with the request.}

\longnewglossaryentry{http:407}
{
  name=407 Proxy Authentication Required,
  type=httpstatus
}
  {The proxy requires \gls{authcred} the client has not supplied.

  See \myrfc{7235}.}

\longnewglossaryentry{http:408}
{
  name=408 Request Timeout,
  type=httpstatus
}
  {The client took too long to finish making the request.}

\longnewglossaryentry{http:409}
{
  name=409 Conflict,
  type=httpstatus
}
  {The request can not be completed due to a conflict in the state of the resource.

  An example of where this could happen is two attempt to modify the same resource taking place.}

\longnewglossaryentry{http:410}
{
  name=410 Gone,
  type=httpstatus
}
  {The requested resource use to exist but it has been removed and will not be replaced.

  This differs from a \gls{http:404} in that clients (and search engines) should not attempt to request the same
  resource again but should treat the resource as permanently purged.

  Legitimate use cases for this response code are rare, but one such use case would be a
  resource that has been removed for legal reasons, such as content found to be in violation
  of obsenity laws or content found to violate intellectual property laws.}

\longnewglossaryentry{http:411}
{
  name=411 Length Required,
  type=httpstatus
}
  {The requested resource requires the client specify the length of its content.}

\longnewglossaryentry{http:412}
{
  name=412 Precondition Failed,
  type=httpstatus
}
  {A request specified precondition can not be met by the server.

  See \myrfc{7232}.}

\longnewglossaryentry{http:413}
{
  name=413 Payload Too Large,
  type=httpstatus
}
  {The request is larger than the server allows.

  See \myrfc{7231}.}

\longnewglossaryentry{http:414}
{
  name=414 URI Too Long,
  type=httpstatus
}
  {The \gls{acr:uri} is larger than what the server allows.

  Usually this results from a \gls{GET} request with too much data encoded within the query string.

  See \myrfc{7231}.}

\longnewglossaryentry{http:415}
{
  name=415 Unsupported Media Type,
  type=httpstatus
}
  {The request includes a media type that is not supported by the server.}

\longnewglossaryentry{http:416}
{
  name=416 Range Not Satisfiable,
  type=httpstatus
}
  {The byte range the client requested can not be supplied by the server.

  Usually this is because the client is asking for bytes beyond the end of the file.

  See \myrfc{7233}.}

\longnewglossaryentry{http:417}
{
  name=417 Expectation Failed,
  type=httpstatus
}
  {The conditions in the \texttt{Expect} header were not able to be met.}

\longnewglossaryentry{http:418}
{
  name=418 I'm a teapot,
  type=httpstatus
}
  {When the client requests the server brew coffee but the server is a tea pot, this
  response code is returned.

  See \myrfc{2324} and \myrfc{7168}.}

\longnewglossaryentry{http:421}
{
  name=421 Misdirected Request,
  type=httpstatus
}
  {The request was directed at a server that is not able to produce a response.

  See \myrfc{7540}.}

\longnewglossaryentry{http:422}
{
  name=422 Unprocessable Entity,
  type=httpstatus
}
  {Used with \gls{webdav}. The request was well-formed but could not be processed due to semantic errors.

  See \myrfc{4918}.}

\longnewglossaryentry{http:423}
{
  name=423 Locked,
  type=httpstatus
}
  {Used with \gls{webdav}. The resource being accessed is locked.

  See \myrfc{4918}.}

\longnewglossaryentry{http:424}
{
  name=424 Failed Dependency,
  type=httpstatus
}
  {Used with \gls{webdav}. The request failed because it depended on another request that failed.

  See \myrfc{4918}.}

\longnewglossaryentry{http:425}
{
  name=425 Too Early,
  type=httpstatus
}
  {Used with \gls{acr:tls} 1.3 \texttt{0-RTT} \gls{sessionresumption}.
  The server will not process the request because it might be used in a replay attack.

  See \myrfc{8470}.}

\longnewglossaryentry{http:426}
{
  name=426 Upgrade Required,
  type=httpstatus
}
  {To access the resource, the client must upgrade to a different protocol.}

\longnewglossaryentry{http:428}
{
  name=428 Precondition Required,
  type=httpstatus
}
  {The server requires the request to be conditional.

  See \myrfc{6585}.}

\longnewglossaryentry{http:429}
{
  name=429 Too Many Requests,
  type=httpstatus
}
  {The client has made too many requests in a given amount of time.

  See \myrfc{6585}.}

\longnewglossaryentry{http:431}
{
  name=431 Request Header Fields Too Large,
  type=httpstatus
}
  {Either a single header, or the sum of all headers, is too large.

  See \myrfc{6585}.}

\longnewglossaryentry{http:451}
{
  name=451 Unavailable for Legal Reasons,
  type=httpstatus
}
  {A legal demand prevents the server from allowing access to the requested
  resource, or to at least one of the resources in a collection of resources.

  This status code is often used as a result of a \gls{acr:dmca} takedown
  notice while the server operator determines whether or not they have a
  legal right to make the resource available.

  It also can be used as a response code when content is restricted in the
  specific geographical area the request came from (e.g.\ government
  censorship.

  See \myrfc{7725}.}

\longnewglossaryentry{http:server-error}
{
  name=5XX Client error response,
  type=httpstatus,
  sort=499x
}
  {\gls{acr:http} response codes that indicate the request can not be completed due to a server error.}

\longnewglossaryentry{http:500}
{
  name=500 Internal Server Error,
  type=httpstatus,
}
  {An error occurred on the server preventing the server from making the requested resource available.

  This is a generic response, when possible it is better to deliver a more specific response unless
  doing so is a security concern.}

\longnewglossaryentry{http:501}
{
  name=501 Not Implemented,
  type=httpstatus
}
  {The request method is not recognized by the server or the server does not yet implement what is
  needed to fulfill the request.}

\longnewglossaryentry{http:502}
{
  name=502 Bad Gateway,
  type=httpstatus
}
  {I like to \emph{jokingly} call this the `NGINX' status code because the vast majority of the time I see it, the
  server it powered by \myhref{https://www.nginx.com/}{NGINX}. Please do not assume that means
  NGINX is a bad server, I know nothing about it and assume the real cause is a configuration
  issue.

  Anyway\ldots{} the response code means the server the resource was requested from is acting as a
  gateway and it received an invalid response from the upstream server.

  I suspect the \emph{real} reason why I see it so often with NGINX has to do with NGINX being the server chosen
  most often for that purpose.}

\longnewglossaryentry{http:503}
{
  name=503 Service Unavailable,
  type=httpstatus
}
  {The services needed for the server to handle the request are currently unavailable.

  This can be used when the database is taken down for maintenance, there is too much load on the server, etc.

  This should be interpreted as a temporary error with the server, try again after a short period of time.}

\longnewglossaryentry{http:504}
{
  name=504 Gateway Timeout,
  type=httpstatus
}
  {The server the request was made to is acting as a gateway server and it did not receive a response from the
  upstream server in the allotted period of time.}

\longnewglossaryentry{http:505}
{
  name=505 HTTP Version Not Supported,
  type=httpstatus
}
  {This response code is sent when the server does not support the version of the \gls{acr:http} protocol the
  client requires.}

\longnewglossaryentry{http:506}
{
  name=506 Variant Also Negotiates,
  type=httpstatus
}
  {When \gls{contentnegotiation} is being used, a configuration error can result in a circular reference.
  This status code is used to specify that type of error on the server has occurred.

  See \myrfc{2295}.}

\longnewglossaryentry{http:507}
{
  name=507 Insufficient Storage,
  type=httpstatus
}
  {Used with \gls{webdav}. The server does not enough storage space to fulfill the request.

  See \myrfc{4918}.}

\longnewglossaryentry{http:508}
{
  name=508 Loop Detected,
  type=httpstatus
}
  {Used with \gls{webdav}. The server detected an infinite loop when attempting to fulfill the request.

  See \myrfc{5842}.}

\longnewglossaryentry{http:510}
{
  name=510 Not Extended,
  type=httpstatus
}
  {Additional extensions are required to fulfill the request.

  See \myrfc{2774}.}

\longnewglossaryentry{http:511}
{
  name=511 Network Authentication Required,
  type=httpstatus
}
  {The client must authenticate to gain access to the network.

  I believe this belongs as a \gls{http:client-error} error code since it is not an error with the server
  but what do I know?

  It is most frequently used with Wi-Fi hotspots that require the user to agree to terms of use before
  granting them use of the network.

  See \myrfc{6585}.}

\longnewglossaryentry{http:invalid}
{
  name=Non-standard response,
  type=httpstatus,
  sort=z000
}
  {Some companies and products make up their own \gls{acr:http} response codes.
  Sometimes it is justified, but generally one should not use an un-official
  status code. Frequently there is an official status code that covers the use case, and if not,
  such an official status code can be added if there is a legitimate use case for it.

  These non-standard response codes are listed is in order of the products that use the non-official response code, when applicable.}

\longnewglossaryentry{http:invalid:103}
{
  name=103 Checkpoint,
  type=httpstatus,
  sort=z000-103
}
  {Used in a resumable request proposal to allow a checkpoint from which to resume aborted
  \gls{PUT} or \gls{POST} requests.}

\longnewglossaryentry{http:apache:218}
{
  name=218 This is fine (Apache),
  type=httpstatus,
  sort=z005-219
}
  {A catch-all error condition that allows the response body to be served when \texttt{ProxyErrorOverride}
  is enabled.

  See the \myhref{\apachemanual{}mod/mod\_proxy.html\#proxyerroroverride}{mod\_proxy} documentation.}

\longnewglossaryentry{http:laravel:419}
{
  name=419 Page Expired (Laravel Framework),
  type=httpstatus,
  sort=z025-419
}
  {The Laravel framework uses this when either a \gls{acr:csrf} token is missing or has expired.}

\longnewglossaryentry{http:spring:420}
{
  name=420 Method Failure (Spring Framework),
  type=httpstatus,
  sort=z040-420
}
  {Deprecated. Spring Framework use to send this response when a method failed. I do not believe
  current versions of the Spring Framework still use this response code.}

\longnewglossaryentry{http:twitter:420}
{
  name=420 Enhance Your Calm (Twitter),
  type=httpstatus,
  sort=z045-420
}
  {Deprecated. Twitter use to send this response when rate-limiting a user. This is sort of an
  easter egg response code, suggesting that the use of Marijuana may help the user calm down.

  Now Twitter uses the standard \gls{http:429} response code for tweet rate-limiting.}

\longnewglossaryentry{http:microsoft:450}
{
  name=450 Blocked by Windows Parental Controls (Microsoft),
  type=httpstatus,
  sort=z028-450
}
  {Used by Windows Parental Controls to indicate it has blocked access to a resource.

  I would like to see a standardized response code for Parental Controls.}

\longnewglossaryentry{http:esri:498}
{
  name=498 Invalid Token (Esri),
  type=httpstatus,
  sort=z015-498
}
  {Used by the Esri ArcGIS server to indicate an invalid token.}

\longnewglossaryentry{http:esri:499}
{
  name=499 Token Required (Esri),
  type=httpstatus,
  sort=z015-499
}
  {Used by the Esri ArcGIS server to indicate a token is required to access the resource, but a token was not supplied.}

\longnewglossaryentry{http:apache:509}
{
  name=509 Bandwidth Limit Exceeded (Apache),
  type=httpstatus,
  sort=z005-509
}
  {Used by Apache and cPanel to indicate the server can not fulfill the request because bandwidth specified by the
  system administrator has been exceeded.

  This is most commonly used in shared hosting environments. It is my not so humble opinion that a \gls{http:503}
  status would be better, it is not any of the requesting client's business that a website has a bandwidth cap.}

\longnewglossaryentry{http:cloudflare:526}
{
  name=526 Invalid SSL Certificate (Cloudflare),
  type=httpstatus,
  sort=z010-526
}
  {Used by Cloudflare and Cloud Foundry to indicate the \gls{x509} certificate used by the upstream origin server
  is not valid.

  It is my \emph{opinion} that \gls{http:503} would be better suited for this.}

\longnewglossaryentry{http:pantheon:530}
{
  name=530 Site is frozen (Pantheon),
  type=httpstatus,
  sort=z030-530
}
  {Used by the Pantheon platform to indicate a site is `frozen' due to a lack of activity.}

\longnewglossaryentry{http:invalid:598}
{
  name=598 Network read timeout error,
  type=httpstatus,
  sort=z000-598
}
  {Used by some proxies to indicate a timeout behind the proxy.

  This use case is \emph{probably} better served with an \gls{http:504} status code.}

\longnewglossaryentry{http:iis:440}
{
  name=440 Login Time-out (IIS),
  type=httpstatus,
  sort=z020-440
}
  {Used by IIS to indicate the session has expired and the user must authenticate again.

  This is useful, I would like to see an official response status for this.}

\longnewglossaryentry{http:iis:449}
{
  name=449 Retry With (IIS),
  type=httpstatus,
  sort=z020-449
}
  {Used by IIS to indicate the request can not be processed because the user has not
  provided the required information.}

\longnewglossaryentry{http:iis:451}
{
  name=451 Redirect (IIS),
  type=httpstatus,
  sort=z020-451
}
  {Used with Exchange ActiveSync to redirect the request to a different resource.

  This is probably better suited with existing \gls{http:redirect} codes.}

\longnewglossaryentry{http:nginx:444}
{
  name=444 No Response (NGINX),
  type=httpstatus,
  sort=z035-444
}
  {Used internally by NGINX to instruct the server to return no data to the client and close
  the connection with the client immediately. This response code is not sent to clients.}

\longnewglossaryentry{http:nginx:494}
{
  name=494 Request header too large (NGINX),
  type=httpstatus,
  sort=z035-494
}
  {NGINX uses this when the request header from the client is too large.

  This seems to duplicate the purpose of the standard \gls{http:431} response code.}

\longnewglossaryentry{http:nginx:495}
{
  name=495 SSL Certificate Error (NGINX),
  type=httpstatus,
  sort=z035-495
}
  {NGINX uses this when there is an error with the \gls{x509} certificate supplied by the client
  as part of the request.

  This is useful, I would like to see an official response status for this.}

\longnewglossaryentry{http:nginx:496}
{
  name=496 SSL Certificate Required (NGINX),
  type=httpstatus,
  sort=z035-496
}
  {NGINX uses this when the server requires a \gls{x509} certificate but the client did not
  provide one.

  This is useful, I would like to see an official response status for this.}

\longnewglossaryentry{http:nginx:497}
{
  name=497 HTTP Request Sent to HTTPS Port (NGINX),
  type=httpstatus,
  sort=z035-497
}
  {NGINX uses this when a request without \gls{acr:tls} is sent to a port that requires TLS.}

\longnewglossaryentry{http:nginx:499}
{
  name=499 Client Closed Request (NGINX),
  type=httpstatus,
  sort=z035-499
}
  {NGINX uses this when the client has closed the connection before the server can send a response.}

\longnewglossaryentry{http:cloudflare:520}
{
  name=520 Unknown Error (Cloudflare),
  type=httpstatus,
  sort=z010-520
}
  {Cloudflare uses this status code when the upstream origin server responds with something
  unexpected.

  A standard \gls{http:502} error might be a better choice.}

\longnewglossaryentry{http:cloudflare:521}
{
  name=521 Web server down (Cloudflare),
  type=httpstatus,
  sort=z010-521
}
  {Cloudflare uses this status code when the upstream origin server refuses the connection
  from cloudlare.

  A standard \gls{http:502} error might be a better choice.}

\longnewglossaryentry{http:cloudflare:522}
{
  name=522 Connection Timed Out (Cloudflare),
  type=httpstatus,
  sort=z010-522
}
  {Cloudflare uses this status code when it is able to connect to the upstream origin server
  but was not able to complete the \gls{acr:tcp} handshake.

  A standard \gls{http:504} error might be a better choice.}

\longnewglossaryentry{http:cloufflare:523}
{
  name=523 Origin Is Unreachable (Cloudflare),
  type=httpstatus,
  sort=z010-523
}
  {Cloudflare was not able to attempt a connection to the upstream origin server.

  A standard \gls{http:503} error might be a better choice.}

\longnewglossaryentry{http:cloudflare:524}
{
  name=524 A Timeout Occurred (Cloudflare),
  type=httpstatus,
  sort=z010-524
}
  {Cloudflare uses this status code when it completes the \gls{acr:tcp} handshake but does
  not response a response in the allotted time.

  A standard \gls{http:504} error might be a better choice.}

\longnewglossaryentry{http:cloudflare:525}
{
  name=525 SSL Handshake Failed (Cloudflare),
  type=httpstatus,
  sort=z010-525
}
  {Cloudflare uses this status code when it is unable to negotiate a \gls{acr:tls} handshake
  with the upstream origin server.}

\longnewglossaryentry{http:cloudflare:527}
{
  name=527 Railgun Error (Cloudflare),
  type=httpstatus,
  sort=z010-527
}
  {Cloudflare uses this status code to indicate a timeout or error after the \gls{acr:wan}
  connection has been established.

  A standard \gls{http:502} error might be a better choice.}

\longnewglossaryentry{http:cloudflare:530}
{
  name=530 Origin DNS Error (Cloudflare),
  type=httpstatus,
  sort=z010-530
}
  {Cloudflare uses this error when it can not resolve the domain name of the upstream
  origin server.

  A standard \gls{http:503} error might be a better choice.}
