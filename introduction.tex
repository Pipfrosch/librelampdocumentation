\chapter{Introduction}
\label{introduction}

This documentation project is currently rough draft and lacks some major sections.

\vspace{2mm}

In 2012, the OpenSSL project added an extension called the Heartbeat extension and they enabled the feature by
default. What heartbeat does, it allows for \gls{acr:tls} over \gls{acr:udp} or with a frequently changing
\gls{acr:ip} address. The vast majority of use cases for OpenSSL had absolutely no need for the extension, yet
it was enabled by default. It can be a useful feature for some users, but the use cases are so few that it
\emph{probably} should be provided by a custom \gls{acr:tls} library for those use cases.

In 2014 it was discovered that the Heartbeat extension in OpenSSL had a serious flaw: It contained a buffer
over-read vulnerability that could be used to compromise the private cryptography key associated with the
server \gls{x509} certificate, or other sensitive data. The vulnerability is described in \mycve{2014-0160}.

The bug was fixed in OpenSSL, but the bug was the proverbial ``straw that breaks the camel's back'' for the
OpenBSD developers. They were already very frustrated with the OpenSSL project, the project ignored many old
bugs while adding new features and retained a lot of legacy code to continue supporting deprecated outdated
systems that made the code more complex than it needed to be.

So OpenBSD forked OpenSSL to create the \myhref{https://libressl.org}{LibreSSL} project. While many \Unix{}
users are very tribal about their particular brand of \Unix{}, the LibreSSL developers understand that a
fork of OpenSSL with better developer leadership would be of benefit to system administrators who use other
operating systems (including Windows) and help those system administrators, so part of the LibreSSL project
involves \myhref{https://github.com/libressl-portable/portable}{LibreSSL Portable} --- code to build LibreSSL
on other modern systems. I am one of those system administrators, my flavor of \Unix{} is the
\myhref{https://www.centos.org/}{CentOS} distribution of \gls{linux}.

I suffer from paranoia. The problem is the paranoia I suffer frequently ends up to be true. I was paranoid that
the U.S.\ government was violating our fourth amendment right and spying on us, many people called me crazy
but the Edward Snowden leaks confirmed it. I had paranoia the \gls{acr:nsa} was inserting \glspl{bd} into
cryptography protocols, it appears exactly that happened with
\myhref{https://blog.0xbadc0de.be/archives/155}{Dual\_EC\_DRBG}. I have paranoia that the massive amount of
user tracking that takes place will be used by nefarious parties to socially engineer the masses. That happened
in 2016 with facebook tracking data and fake news in a (possibly successful) attempt to change the outcome
of the election by socially engineering the voters.

I also have paranoia that \gls{bd} are added to popular Open Source projects by adding features most people
do not need (so the code is not as carefully analyzed and the `many eyes' paradigm fails) yet the feature is
enabled allowing it to be exploited. I do not know that the Heartbleed vulnerability is an example of an
intentional \gls{bd} so it would not be fair for me to claim such, but it certainly is a demonstration that
the OpenSSL project was sucsceptible to that kind of \gls{bd} insertion. Hopefully they no longer are but I
do not know that.

The philosophy of the LibreSSL developers was much more inline with the way I thought a \gls{acr:tls} library
should be developed. Using LibreSSL for the OpenSSL API reduces my paranoia.

I first packaged LibreSSL for CentOS 7 on 2015 August 03 (LibreSSL 2.2.1) and have been building the
Open Source server packages I use against LibreSSL ever since. I call the project LibreLAMP with `Libre'
referencing both LibreSSL and the general concept of `Free as in Freedom -- Liberty -- Libre' and in reference
to the acronym `Linux Apache MariaDB PHP' (the M use to stand for MySQL but\ldots{})

\section{Software Version Philosophy}

I like a \textbf{stable} operating system. This is why I choose CentOS as my \Unix{} flavor of choice.
The software in enterprise \gls{linux} distributions is not bleeding edge, but many of the operational bugs have
been fixed, enterprise distributions are stable. They also are maintained for a long time, so one does
not have to start with a new operating system version very often, they can take their time to migrate to
new versions and wait for the major bugs to be fixed.

However with a server stack that philosophy does not really work. There frequently are features in fresh
`bleeding edge' versions of server software that improve security and functionallity.

This is why the server software in LibreLAMP is not just a rebuild of CentOS 7 packages against LibreSSL
but is far more `bleeding edge' than what ships in CentOS 7.

The current version of Apache allows for \gls{acr:http2}. The current version of MariaDB has significant
performance benefits, as well as additional engines that are beneficial to enterprise database needs.
The current version of \gls{acr:php} has \emph{numerous} enhancements over the crusty version in
CentOS 7. Furthermore, many web application developers do not have the financial resources to support
old versions of PHP so to keep web applications up to date, you \emph{need} a modern PHP.

When a system administrator runs into an issue that needs to be solved, the project mailing lists are a
very useful resource. You are far more likely to get help if you are using a modern version of the
project.

This is why LibreLAMP is mostly modern software that installs on top of enterprise distribution that
has mostly older software.

\section{FIPS Disclosure}

LibreSSL is not \gls{acr:fips} certified. The project has no desire to become FIPS certified.

\gls{acr:fips} is very problematic. FIPS certification did not prevent the heartbleed
vulnerability. FIPS certified the Dual\_EC\_DRBG \gls{acr:pRNG} \emph{despite} the fact that
knowledgeable cryptographers publicly expressed concerns a \gls{bd} was present
\emph{before} FIPS specified it as one of
a few \gls{acr:pRNG} options that FIPS certified software could use.

While that \gls{pRNG} is no longer part of \gls{acr:fips}, for a period of about four years
systems that wanted to be FIPS certified were \emph{required} to provide that pRNG \emph{despite}
the existing backdoor concerns we now know to be true. FIPS is dangerous in my opinion.

Not all \gls{acr:fips} certified software used that \gls{pRNG} but it still should not have
been required to be there.

The LibreSSL project is interested in \emph{actual} security, not the false sense of security
that results from \gls{acr:fips} certification.

Do not use LibreLAMP if you are \underline{required} to have \gls{acr:fips} certification,
you will not meet the requirement. It may be a worthless requirement, but I believe it is my
obligation to make sure lack of FIPS certification in LibreLAMP build of cryptography
related software is known.

The paranoid part of me wonders if the false sense of security that comes from \gls{acr:fips}
certification is exactly what the government wants. Comfortable ignorance to potential
\glspl{bd}.

\section{TLS 1.3 Disclosure}

At this time, LibreSSL does not support \gls{acr:tls} 1.3. That may change by the time this
documentation is finished.

The LibreSSL developers made the decision not to start working on implementing \gls{acr:tls} 1.3
until the draft was finalized. That actually is a decision I respect, even though it does result
in a delay in implementation. It is being worked on now.

The updated GnuTLS (see section~\ref{sec:introduction:gnutls} on page~\pageref{sec:introduction:gnutls})
packaged in LibreLAMP does support \gls{acr:tls} 1.3, as does LibreLAMP software that links against
it, including the Exim \gls{acr:smtp} server, the \gls{acr:gnu} Wget \gls{acr:http} client, and the
experimental GNU Wget2 HTTP client.

The main benefit of \gls{acr:tls} 1.3 is that it simplifies secure TLS implementation by
\emph{requiring} things like \gls{acr:fs} and \gls{acr:aead} cipher suites. We can implement
those same restrictions manually for \gls{acr:tls} 1.2 and will have to anyway even when LibreSSL
does implement TLS 1.3 simply because it will be some time before all clients support TLS 1.3.

\section{System Libraries}

In addition to building updated server software, LibreLAMP updates some libraries. For example,
the \gls{acr:php} module \myhref{https://pecl.php.net/package/imagick}{Imagick} can not
create \myhref{https://developers.google.com/speed/webp/}{WebP} images in PHP unless the
version of \myhref{https://imagemagick.org/index.php}{ImageMagick} it is built against is new
enough to support WebP. The version of ImageMagick in CentOS 7 is too old, so LibreLAMP updates
it to the current version.

This results in a shared library version change, so packages in CentOS 7 and \gls{acr:epel}
that link against the older ImageMagick library (such as \myhref{https://inkscape.org/}{Inkscape})
have to be rebuilt to use the newer updated shared library.

This is why you will find many packages in LibreLAMP that seemingly have nothing to do with
a server stack.

The alternative is to build compatibility packages with the correct version of the shared
libraries those packages need and in \emph{some} cases I do that. For example, I do not rebuild
every single piece of software in CentOS 7 and \gls{acr:epel} that link against MariaDB but
instead I choose to provide a compatibility package that does provide the older shared library
versions those packages need.

\section{GnuTLS}
\label{sec:introduction:gnutls}

The OpenSSL API is not the only game in town, and honestly I am not sure it is the best. A lot of server
software chooses that API hence the need for a library that provides it, but diversity is good.

CentOS 7 ships with \myhref{https://gnutls.org/}{GnuTLS} but it is an older version. LibreLAMP has
provided a rebuild of that version (needed because LibreLAMP upsates \myhref{https://nlnetlabs.nl/projects/unbound/about/}{unbound}
resulting in a shared library incompatibility if the CentOS version of GnuTLS is not rebuilt
against the newer Unbound) but in December 2018 I also started providing the current version
of GnuTLS provided as a compatibility package with an install prefix of \texttt{/opt/gnutls}.

Having the current version of GnuTLS available allows me to provide an updated version of
\myhref{https://www.gnu.org/software/wget/}{GNU Wget} that includes \gls{acr:tls} 1.3 support.

While \texttt{wget} is not specifically server software, it frequently is used on servers
and having a modern version with \gls{acr:http2} and \gls{acr:tls} 1.3 support is beneficial.

Additionally, the modern version of GnuTLS allows for building the modern version of Exim
with improved \gls{acr:dkim} support.

\section{Mail Stack}

The \myhref{http://www.postfix.org/}{Postfix} that ships in CentOS 7 is too old for \gls{acr:dane} support. DANE is very important
for user privacy, so LibreLAMP includes an updated version of Postfix with DANE support.

I do not like how centralized e-mail service has become. A very large percentage of all
e-mail passes through Google, Microsoft, or Yahoo controlled mail servers. The opportunity
for abuse is very high, and tech companies seem to \emph{always} believe their violation
of user privacy is justified.

It is not just tech companies, Kamala Harris said that when she had low income parents
arrested because their children missed too many days of school, she was actually
helping them. Pete Buttigieg said that when he demolished houses in low income
neighborhoods, he did so to combat the drug problems in those neighborhoods.

Elites in our society very frequently believe they are justified in violating our rights.
It is a serious problem and is why centralization of e-mail services is very dangerous.
If that centralization is not being abused now, it certainly will be in the future, I
guarantee it.

LibreLAMP provides a modern of Dovecot so that those who wish to run their own mail
box services and avoid the centralization taking place can do so with a modern
Dovecot linked against LibreSSL.

I have started to become frustrated with the Postfix project. It seems almost every time
there is an update to either LibreSSL or Postfix, something breaks. Often Postfix
still builds and unit tests pass, but there is still breakage that I do not detect
until I view the mail logs.

The Postfix Developers are not very concerned about remaining compatibility with
the LibreSSL fork of OpenSSL and to be completely honest, it would not be fair for
me to expect them to. It is frustrating though, attempting to ask for help on their
list is met by developers responding `Use OpenSSL'.

\myhref{http://exim.org/}{Exim} in very committed to support for the GnuTLS API,
so LibreLAMP is currently providing builds of the current version of Exim built
against GnuTLS.

I \emph{personally} still use Postfix for everything Port 25 related, Postfix has a
lot of very useful features including but limited to Postscreen that I do not yet
know how to accomplish in Exim. However I am now switching to Exim for web servers
that need to send e-mail but do not listen on Port 25.

\section{DNS Stack}

Every server on the Internet, \emph{every single one}, really should run a local
\gls{acr:dnssec} enforcing caching name server. This is critical to security.

LibreLAMP includes the current version of \myhref{https://nlnetlabs.nl/projects/unbound/about/}{Unbound}
to provide this.

For those who wish to run their own Authoritative \gls{acr:dns} server, LibreLAMP
includes the current version of \myhref{https://www.nlnetlabs.nl/projects/nsd/about/}{NSD}.

Both Unbound and NSD have a \emph{much} better security record than BIND.

LibreLAMP also includes some updated \gls{acr:dns} related libraries so that some
of the recently added record types are better supported with zone file validation tools.

\section{Other Servers}

In addition to the stacks mentions, some other server software (e.g, ProFTPD) have
been built using LibreSSL for CentOS 7.

\section{CentOS 8}

\gls{acr:rhel} 8 has been released, so CentOS 8 is not far behind and will likely be released
before the book is finished.

I always wait until after the first major update before I play with a new major version of
CentOS. That reduces the cussing.

When I do start playing with CentOS 8, it will take a little while for me to digest the
differences so I can create competent \gls{acr:rpm} spec files that build packages for
both CentOS 7 and CentOS 8.

There also is a matter of money. I do not like the idea of building packages for systems
I do not use, I will need a CentOS 8 workstation. Workstation means XEON processor so I
can use ECC memory to avoid flipped bits from corrupting a package build.

Having an actual CentOS 8 workstation is necessary for proper testing of fixes and for
dealing with obscure build issues.

Donations would be appreciated, see the back cover (page~\pageref{backcover}).

