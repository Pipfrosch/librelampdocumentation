\chapter{Alice's Web Developer Password Recommondations}

This appendix is my \emph{opinion}. Take it with a grain of a salt, or even a
32-bit \gls{salt}.

This is intended for web application developers who will be including the handling
of password-based \gls{authcred} in their web applications.

Please encourage your users to use a \gls{passmanager}. Some of us do not like
them, but the bottom-line is they do result in a tendency for users to use better
passwords and to use \emph{unique} passwords for each site, and that is a good thing.

The blog maintained by \myhref{https://pthree.org/}{Aaron Toponce} has excellent reviews
of password managers if you need assistance in choosing one to recommend to your users.

\section{Password Rules}

Do not be overly specific with the rules for passwords, they will frustrate users and
give system crackers hints for how to implement dictionary attacks on a dump of your
database if they manage to get such a dump.

However there do need to be some rules.

\subsection{Mininum Length}
For regular users, the minimum password length you allow should not be less than 8 and
should not be more than 12.

For administrative accounts, the mininum password length you allow should not be less
that 12 and should not be more than 16.

The justification for this: If your minimum password length is too short, many users
will use really short passwords that are easy to brute force even if they do not form
dictionary words.

On the other hand if the minimum length is too long, users will tend to use passwords
that are of the right length but easy for them to remember (\emph{e.g. \texttt{myDogIsNamedR0ver}})
and easy for people who know them to figure out.

For administrative accounts, a slightly longer minimum length is warranted.

\subsection{Maximum Length}
If you set a maximum length at all, it should be absurdly long. A standard terminal window
is 80 characters wide, so I would suggest any maximum length you impose should not be less
than 80 characters.

\subsection{Character Restriction}
Do not forbid any characters in the printable 7-bit ASCII range. Feel free to forbid
control characters if you want.

If you allow characters beyond the 7-bit ASCII range, make sure all your login forms
use UTF-8 to submit the password to your server.

\subsection{Character Variety}
I define five groups of characters:

\begin{description}
\item[A--Z] The upper case ASCII letters A through Z.
\item[a--z] The lower case ASCII letters a through z.
\item[0--9] The decimal ASCII numbers.
\item[Special ASCII Characters] The printable 7-bit ASCII characters that are not alpha-numeric.
\item[Extended Characters] These are characters beyong the 7-bit ASCII range. This group is only
applicable if you allow passwords to contain characters beyond the 7-bit ASCII range.
\end{description}

Each password, in my opinion, should be required to have at least one character from at least three of
the above groups. Which three should be up to the user.

\bigskip
\textbf{NOTE:} Current \gls{acr:nist} guidelines actually recommend against this. The argument is that
it still results in weak passwords, like \texttt{123AliceWonder456}. Dammit, did I just publish that?
Oh shitake mushrooms, time to change my \texttt{root} password\ldots{} \winkingfaceemoji{}

Instead it is recommended to use a dictionary and strength test. However I do not see why they are mutually
exclusive. However I do concede that if you do use a Dictionary and Strength test, this rule may not
be necessary.

\subsection{No Formatting Rules}

Some websites make absurd rules like `At least one upper case letter that is not the first letter' or
`at least one number that has at least one letter on either side of it'.

These crazy rules sometimes interfere with the operation of \glspl{passmanager} that produce very
secure passwords that happen to not match the defined rule set.

\subsection{Dictionary and Strength Check}
At a minimum, passwords should be checked against a dictionary of the most commonly used passwords
before they are allowed.

A \gls{acr:php} class for accomplishing this can be found at
\myhref{https://www.the-art-of-web.com/php/password-strength/}{The Art of Web}.

Unfortunately that class involves the \gls{acr:php} \texttt{exec()} function which I personally do not
allow on my systems \emph{especially} in conjunction with user provided input. I will attempt to code
something that does not need \texttt{exec()}.

\section{Password Resets}

This is something a lot of web applications get very wrong. In my arrogant opinion.

\subsection{Do Not Expire Passwords}

Do not force users to change their password just because they have not done so in awhile. Just don't.

If a user is using the same password at multiple sites, all forcing them to change it will accomplish
is they will change it to another password they commonly use.

\subsection{Do Not Save Old Hashes}

This practice really annoys me. Some websites when you change your password will save the hash from
your previous password.

When a user changes their password, it should be assumed that it was changed for a reason, and that
reason could be that it was possibly compromised.

Saving the hash from their former passwords serves no real-world purpose. It can not be used for
recovery purposes because it may be compromised.

Some websites use former hashes to forbid a user from changing their password to one they used
before.

Many of us however, when we have a need to log in from hardware we do not fully trust, will do a
password reset and use a new password temporarily so that we never enter our \emph{real} password
on the hardware we are absolutely sure is free of keystroke sniffers etc.

When we no longer are using that hardware, we then change it back to our \emph{real} password. It
is rather frustrating when we are not allowed to.

For web applications that do retain hashes of the old password, do they faithfully change the hash
by putting it through more rounds of hashing as \acrshort{acr:gpu} power increases? \thinkingfaceemoji{}

If not, saving the old hashes is putting their users at risk.

\subsection{SMS Password Reset}

When a user has forgotten their password, some web applications allow them to reset the password
by simply sending an \gls{acr:sms} message with a token code to their phone to verify they are who
they claim to be.

Do not do this. The \gls{acr:sms} system was not designed to be a secure means of identity authentication
and attacks on the system exist that allow someone else to intercept messages sent to someone else's
phone number.

Send an e-mail and make damn sure your outgoing \gls{acr:smtp} supports \gls{acr:tls}.

\subsection{Password Generation}
For users that do not use a \gls{passmanager} to generate their passwords, your web application should have the ability to
generate a strong secure password for them. Make sure it uses a cryptography quality \gls{pRNG}.

For a basic generator in \gls{acr:php}:

\begin{phpCodeListing}
<?php
$raw = random_bytes(16);
$generated = str_shuffle(base64_encode($raw));
\end{phpCodeListing}
%stopzone

Note the only purpose the \texttt{str\_shuffle()} is to take the ending `\texttt{==}' that is always at the end of a
\texttt{base64\_endode()} with 16 byte input and put those padding characters mixed into the rest of the string. You
could also trim them or leave them, whatever floats your boat.

That is just a very basic password generator, there are some really neat ones out there you can also use. Just make
sure they use a cryptography quality \gls{acr:pRNG}.

\section{Password Hash Algorithm}

Most \glspl{cryptohash} are not suitable, at least on their own, for password hashing.
